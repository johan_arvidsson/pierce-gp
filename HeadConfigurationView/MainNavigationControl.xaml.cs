﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Aten.Extensions;
using Aten.AS.Extensions;
using System.ComponentModel.Composition;
using Aten.Extensions.Keyboard;

namespace HeadConfigurationView
{
    [Export(typeof(IPluginMenuView))]
    public partial class MainNavigationControl : UserControl, IPluginMenuView, IKeyboardDialogFactoryContainer, IPageManagerContainer
    {
        [Import(typeof(IKeyboardDialogFactory))]
        public IKeyboardDialogFactory KeyboardDialogFactory { get; set; }

        [Import]
        public IPageManager PageManager { get; set; }

        private static Page[] pages;
        private Page[] Pages
        {
            get { return pages ?? (pages = new Page[] { CreatePage<ConfigurationPage1>(), CreatePage<ConfigurationPage2>(), CreatePage<ConfigurationPage3>(), CreatePage<ConfigurationPage4>(), CreatePage<ConfigurationPage5>(), CreatePage<ConfigurationPage6>() }); }
        }

        #region IPluginMenuView
        private static string[] titles;
        public string[] Titles
        {
            get { return titles ?? (titles = Pages.Select(p => p.Title).ToArray()); }
        }

        private static string[] uris;
        public string[] Uris
        {
            get { return uris ?? (uris = Pages.Select(p => "/" + p.GetType().Namespace + ";component/" + p.GetType().Name + ".xaml").ToArray()); }
        }

        FrameworkElement IPluginMenuView.View
        {
            get { return this; }
        }

        public void Navigate(string uri)
        {
            int index = Array.IndexOf(Uris, uri);

            frame.Navigate(Pages[index]);
        }
        #endregion

        public MainNavigationControl()
        {
            InitializeComponent();
            frame.NavigationUIVisibility = NavigationUIVisibility.Hidden;
        }
        
        private T CreatePage<T>() where T : Page, new()
        {
            var pageidentifier = typeof(T).ToString();

            PageManager.ChangeCurrentPage(pageidentifier);

            var page = new T();
            page.Loaded += (s, e) => { PageManager.ShowPage(pageidentifier); };

            return page;
        }
    }
}
