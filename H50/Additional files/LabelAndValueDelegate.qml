import QtQuick 2.8
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.1
import MessageHandler 1.0

import "qrc:/Controls"

Rectangle {
    implicitWidth: Style.displayWidth
    implicitHeight: 65
    color: Style.colorLight
    property bool loaded: false
    property bool storedCheckedValue
    property string displayValue

    H50Text {
        id : labelText
        anchors { top: parent.top; left: parent.left; margins: Style.displayMargin; leftMargin: Style.displayMargin * 8 }
        width: parent.width / 2
        text: model ? model.description : ""
        font.pointSize: 16
    }

    H50Text {
        id : valueText
        anchors { top: parent.top; left: labelText.right; margins: Style.displayMargin; leftMargin: Style.displayMargin * 8 }
        width: parent.width / 2
        text: displayValue
        font.pointSize: 16
    }

    Rectangle { Layout.fillWidth: true }


}

