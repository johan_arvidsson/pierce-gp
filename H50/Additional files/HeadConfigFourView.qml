import QtQuick 2.8
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.1
import MessageHandler 1.0


import "qrc:/Controls"
import "qrc:/Controls/Configuration"

Component {
    Rectangle {
        id: root
        objectName: "HeadConfigFourView"
        width: 800
        height: Style.displayHeight
        color: Style.colorLight

        property int activeView: 0
        property bool activateLoaders: false

        function toggleContextMenu() {
            drawer.toggle()
        }

        QtObject {
            id: inputModel
            objectName: "inputModel"

            property string currentInSignal
            property string currentInSignalPath: "Customer.BasAggregat.Map_Objects.In.*.Active"

            property string currentOutSignal
            property string currentOutSignalPath: "Customer.BasAggregat.Map_Objects.Out.*.Active"
        }

        ListModel {
            id: contextMenuModel

            ListElement { title: qsTr("Timers/speed") }


        }

        Connections {
            target: root

            onActiveViewChanged:  {
                listView.currentIndex = activeView
                swipeviewElement.currentIndex = activeView

                previousViewPageIndex = activeView
                currentViewPage = contextMenuModel.get(activeView).title
            }
        }

        NavigationDrawer {
            id: drawer
            anchors { top: parent.top; bottom: parent.bottom }
            width: maxWidth + 4 * Style.displayMargin > parent.width * 0.25 ? maxWidth + 4 * Style.displayMargin : parent.width * 0.25
            visualParent: parent
            position: Qt.RightEdge
            color: Style.colorLight
            property int maxWidth: 0

            ListView {
                id: listView
                anchors { fill: parent; topMargin: Style.displayMargin * 4 }
                clip: true
                focus: true
                spacing: 10
                model: contextMenuModel

                delegate: Rectangle {
                    width: parent.width
                    height: Style.defaultRowHeight
                    color: "transparent"

                    H50Text {
                        id: title
                        anchors { horizontalCenter: parent.horizontalCenter }
                        height: parent.height
                        verticalAlignment: Text.AlignVCenter
                        text: model.title
                        color: (listView.currentIndex === index) ? Style.colorTextMarked : Style.colorText
                        font.pointSize: 18

                        onPaintedWidthChanged: {
                            if (paintedWidth > drawer.maxWidth)
                                drawer.maxWidth = paintedWidth
                        }
                    }

                    MouseArea {
                        anchors.fill: parent

                        onClicked: {
                            listView.currentIndex = index
                        }
                    }
                }

                highlight: Rectangle {
                    anchors { left: parent.left; right: parent.right }
                    color: Style.colorMarked
                    focus: true
                }

                onCurrentIndexChanged: {
                    drawer.hide()
                    activeView = currentIndex
                }
            }
        }

        SwipeView {
            id: swipeviewElement
            anchors { top: parent.top; bottom: pageIndicator.top }
            height: Style.displayHeight
            width: 800

            onCurrentIndexChanged: {
                activeView = currentIndex
            }

            Item {
                id: page1

                ListModel {
                    id: listModelPage1

                    ListElement{
                        elementType: "qrc:/Controls/H50DividerAndHeaderDelegate.qml"
                        description: qsTr("Butt saw")
                    }

                    ListElement{
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible : false
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 400
                        initOrStorageValue: 0
                        description: qsTr("Saw motor speed (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Buttsaw_Speed_mA"
                        helpTextBody: qsTr("Sets the max speed of the butt saw motor")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible : false
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 300
                        initOrStorageValue: 0
                        description: qsTr("Saw bar out speed (%)")
                        dobPath:"Customer.BasAggregat.Configuration_Variables.CV_Buttsaw_Out_Speed_mA"
                        helpTextBody: qsTr("Sets the max speed of the bar out")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible : false
                        minValue: 0
                        maxValue: 300
                        defaultValue: 150
                        initOrStorageValue: 0
                        description: qsTr("Delay saw bar out (ms)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Buttsaw_DelaySwordOut_ms"
                        helpTextBody: qsTr("Time between start of saw motor and saw bar out (ms)")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible : false
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 300
                        initOrStorageValue: 0
                        description: qsTr("Saw bar retract speed (%)")
                        dobPath:"Customer.BasAggregat.Configuration_Variables.CV_Buttsaw_Retract_Speed_mA"
                        helpTextBody: qsTr("Sets the max retract speed of the bar")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/H50DividerAndHeaderDelegate.qml"
                        description: qsTr("Top saw")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible : false
                        minValue: 0
                        maxValue: 300
                        defaultValue: 150
                        initOrStorageValue: 0
                        description: qsTr("Saw motor speed (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_TopSaw_Speed_mA"
                        helpTextBody: qsTr("Sets the max speed of the top saw motor")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible : false
                        minValue: 0
                        maxValue: 300
                        defaultValue: 150
                        initOrStorageValue: 0
                        description: qsTr("Saw bar out speed (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_TopSaw_Out_Speed_mA"
                        helpTextBody: qsTr("Sets the max speed of the bar out")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible : false
                        minValue: 0
                        maxValue: 300
                        defaultValue: 150
                        initOrStorageValue: 0
                        description: qsTr("Delay saw bar out (ms)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_TopSaw_DelaySwordOut_ms"
                        helpTextBody: qsTr("Time between start of saw motor and saw bar out (ms)")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible : false
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 300
                        initOrStorageValue: 0
                        description: qsTr("Saw bar retract speed (%)")
                        dobPath:"Customer.BasAggregat.Configuration_Variables.CV_TopSaw_Retract_Speed_mA"
                        helpTextBody: qsTr("Sets the max retract speed of the bar")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible : false
                        minValue: 0
                        maxValue: 300
                        defaultValue: 150
                        initOrStorageValue: 0
                        description: qsTr("Automatic sawp to top saw diameter limit (mm)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Topsaw_DiameterLimitSwapToTopsaw"
                        helpTextBody: qsTr("Saw selection swaps to \"Top saw mode\" while feeding and when this diameter is reached.")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/H50DividerAndHeaderDelegate.qml"
                        description: qsTr("General settings")
                    }


                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible : false
                        minValue: 0
                        maxValue: 300
                        defaultValue: 150
                        initOrStorageValue: 0
                        description: qsTr("Saw bar retract standby (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Saws_MinRetractSpeed_mA"
                        helpTextBody: qsTr("Sets the minimum beginning retract speed of the bar")
                    }


                    ListElement {
                        elementType: "qrc:/Controls/H50ComboBoxDelegate.qml"
                        initOrStorageValue: 0
                        description: qsTr("Saw toggle key funcyion")
                        item0: qsTr("Swap saw")
                        item1: qsTr("Disabled")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Bucking_ToggleSawFuncDisabled"
                    }
                }

                ListView {
                    id: listViewPage1
                    anchors.top: parent.top
                    width: parent.width
                    height: parent.height
                    spacing: 25
                    model:listModelPage1
                    clip: true

                    ScrollBar.vertical: ScrollBar { visible: parent.contentHeight > parent.height; width: 5; anchors.right: parent.right; anchors.rightMargin: Style.displayMargin; policy: ScrollBar.AlwaysOn }

                    delegate:Item {
                        id: delegateComponent

                        property var itemModel : model
                        height: childrenRect.height

                        Loader{
                            id: loaderPage1
                            active: activateLoaders
                            source: model.elementType

                            property bool swipeDelegateSwiping
                            property var model: parent.itemModel
                            property var viewModel: headConfigFourViewModel ? headConfigFourViewModel : undefined
                            asynchronous: (activeView === 0) ? false : true

                            onSwipeDelegateSwipingChanged: {
                                if (swipeDelegateSwiping)
                                    swipeviewElement.interactive = false
                                else
                                    swipeviewElement.interactive = true
                            }
                        }
                    }
                }
            }                        
        }

        H50PageIndicator {
            id: pageIndicator
            count: swipeviewElement.count
            currentIndex: swipeviewElement.currentIndex

            anchors.bottom: parent.bottom
            anchors.horizontalCenter: parent.horizontalCenter
        }

        Component.onCompleted: { viewModelHandler.addViewModel(objectName + "Model", root); activateLoaders = true; currentViewPage = contextMenuModel.get(activeView).title; previousObjectName = objectName }
        Component.onDestruction: {
            previousViewPageIndex = activeView


            viewModelHandler.removeViewModel(objectName + "Model")
        }
    }
}
