import QtQuick 2.8
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.1
import MessageHandler 1.0
import QtMultimedia 5.8



import "qrc:/Controls"
import "qrc:/Controls/Configuration"
import "qrc:/Views/Adaptable"

Component {        
    Rectangle {
        id: root
        objectName: "RootViewExtension"
        width: Style.displayWidth
        height: Style.displayHeight
        color: Style.colorLight

        property int activeView: 0
        property bool activateLoaders: false        

        QtObject {
            id: inputModel
            objectName: "inputModel"


            property int hmi_CutMainWindowActive
            property string hmi_CutMainWindowActivePath: "Customer.BasAggregat.Interface_Variables.HMI_Sound_InMainCutWindow.Value"

            property int hmi_CutTopWindowActive
            property string hmi_CutTopWindowActivePath: "Customer.BasAggregat.Interface_Variables.HMI_Sound_InTopCutWindow.Value"

            property int hmi_TopSawSelected
            property string hmi_TopSawSelectedPath: "Customer.BasAggregat.Interface_Variables.HMI_Sound_TopSawSelected.Value"

            property int hmi_StemReg
            property string hmi_StemRegPath: "Customer.BasAggregat.Interface_Variables.HMI_Sound_StemReg.Value"

            property int hmi_WrongManSel
            property string hmi_WrongManSelPath: "Customer.BasAggregat.Interface_Variables.HMI_Sound_WrongManSel.Value"

            property int hmi_EndOfTree
            property string hmi_EndOfTreePath: "Customer.BasAggregat.Interface_Variables.HMI_Sound_EndOfTree.Value"

            property int hmi_DiaReset
            property string hmi_DiaResetPath: "Customer.BasAggregat.Interface_Variables.HMI_Sound_DiaReset.Value"





            onHmi_CutMainWindowActiveChanged: {
                if (hmi_CutMainWindowActive == 1){
                    cutMainWindowSound.volume = parseInt(rootViewModel.getVolume())/100
                    cutMainWindowSound.play()
                }
            }

            onHmi_CutTopWindowActiveChanged: {
                if (hmi_CutTopWindowActive == 1){
                    cutTopWindowSound.volume = parseInt(rootViewModel.getVolume())/100
                    cutTopWindowSound.play()
                }
            }

            onHmi_TopSawSelectedChanged: {
                if (hmi_TopSawSelected == 1){
                    topSawSelectedSound.volume = parseInt(rootViewModel.getVolume())/100
                    topSawSelectedSound.play()
                }
            }

            onHmi_StemRegChanged: {

                if (hmi_StemReg == 1){
                    stemRegSound.volume = parseInt(rootViewModel.getVolume())/100
                    stemRegSound.play()
                }

            }

            onHmi_WrongManSelChanged: {
                if (hmi_WrongManSel == 1){
                    wrongManSelSound.volume = parseInt(rootViewModel.getVolume())/100
                    wrongManSelSound.play()
                }
            }

            onHmi_EndOfTreeChanged: {
                if (hmi_EndOfTree == 1){
                    endOfTreeSound.volume = parseInt(rootViewModel.getVolume())/100
                    endOfTreeSound.play()
                }
            }

            onHmi_DiaResetChanged: {
                if (hmi_DiaReset == 1){
                    diaResetSound.volume = parseInt(rootViewModel.getVolume())/100
                    diaResetSound.play()
                }

            }




            property bool cameraShow
            property string cameraShowPath: "OperatorPanel.Camera.Show"

            onCameraShowChanged: {
                if (cameraShow)
                    rootViewModel.enableCameraStream()
                else
                    rootViewModel.disableCameraStream()
            }
        }

        SoundEffect{
            id: cutMainWindowSound
            source: "Sounds/cutWindowMainSaw.wav"
            loops: 0
            volume: {if (rootViewModel)
                        return parseInt(rootViewModel.getVolume())/100;
            }

        }

        SoundEffect{
            id: cutTopWindowSound
            source: "Sounds/cutWindowTopSaw.wav"
            loops: 0
            volume: {if (rootViewModel)
                        return parseInt(rootViewModel.getVolume())/100;
            }

        }

        SoundEffect{
            id: topSawSelectedSound
            source: "Sounds/topSawSelected.wav"
            loops: 0
            volume: {if (rootViewModel)
                        return parseInt(rootViewModel.getVolume())/100;
            }

        }

        SoundEffect{
            id: stemRegSound
            source: "Sounds/stemRegistred.wav"
            loops: 0
            volume: {if (rootViewModel)
                        return parseInt(rootViewModel.getVolume())/100;
            }

        }

        SoundEffect{
            id: wrongManSelSound
            source: "Sounds/wrongManulSelection.wav"
            loops: 0
            volume: {if (rootViewModel)
                        return parseInt(rootViewModel.getVolume())/100;
            }

        }

        SoundEffect{
            id: endOfTreeSound
            source: "Sounds/endOfTree.wav"
            loops: 0
            volume: {if (rootViewModel)
                        return parseInt(rootViewModel.getVolume())/100;
            }
        }

        SoundEffect{
            id: diaResetSound
            source: "Sounds/diameterReset.wav"
            loops: 0
            volume: {if (rootViewModel)
                        return parseInt(rootViewModel.getVolume())/100;
            }
        }






        Timer{

            id: displayAliveTimer
            repeat: true
            interval: 500
            triggeredOnStart: true
            running: true

            property int watchDogValue : 0

            onTriggered: {
                watchDogValue = (watchDogValue == 0) ? 1 : 0
                if (rootViewExtensionModel && rootViewExtensionModel.connected)
                    rootViewExtensionModel.setAttribute("Customer.BasAggregat.Interface_Variables.HMI_System_H50WatchDog.Value", watchDogValue)
            }
        }


        Component.onCompleted: {
            viewModelHandler.addViewModel(objectName + "Model", root);
            activateLoaders = true;
        }
        Component.onDestruction: {
            viewModelHandler.removeViewModel(objectName + "Model")
        }
    }
}
