import QtQuick 2.8
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.1
import MessageHandler 1.0
import "qrc:/Controls"

Rectangle{

        implicitWidth: Style.displayWidth
        implicitHeight: 65
        color: Style.colorLight
        anchors { top: parent.top; left: parent.left; margins: Style.displayMargin; leftMargin: Style.displayMargin * 8 }



        H50Button{
            text: model.description
            onPressed:  {
                if (viewModel)
                    viewModel.setAttribute(model.dobPath + ".Value", 1)
            }

            onReleased: {
                if (viewModel)
                    viewModel.setAttribute(model.dobPath + ".Value", 0)
            }
        }
}



