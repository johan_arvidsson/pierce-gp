import QtQuick 2.8
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.1
import MessageHandler 1.0

import "qrc:/Controls"
import "qrc:/Controls/Configuration"

Component {
    Rectangle {
        id: root
        objectName: "HeadConfigThreeView"
        width: 800
        height: Style.displayHeight
        color: Style.colorLight

        property int activeView: 0
        property bool activateLoaders: false

        function toggleContextMenu() {
            drawer.toggle()
        }

        QtObject {
            id: inputModel
            objectName: "inputModel"

            property string currentInSignal
            property string currentInSignalPath: "Customer.BasAggregat.Map_Objects.In.*.Active"

            property string currentOutSignal
            property string currentOutSignalPath: "Customer.BasAggregat.Map_Objects.Out.*.Active"

        }

        ListModel {
            id: contextMenuModel

            ListElement { title: qsTr("Speed") }
            ListElement { title: qsTr("Jamming") }
        }

        Connections {
            target: root

            onActiveViewChanged:  {
                listView.currentIndex = activeView
                swipeviewElement.currentIndex = activeView

                previousViewPageIndex = activeView
                currentViewPage = contextMenuModel.get(activeView).title
            }
        }

        NavigationDrawer {
            id: drawer
            anchors { top: parent.top; bottom: parent.bottom }
            width: maxWidth + 4 * Style.displayMargin > parent.width * 0.25 ? maxWidth + 4 * Style.displayMargin : parent.width * 0.25
            visualParent: parent
            position: Qt.RightEdge
            color: Style.colorLight
            property int maxWidth: 0

            ListView {
                id: listView
                anchors { fill: parent; topMargin: Style.displayMargin * 4 }
                clip: true
                focus: true
                spacing: 10
                model: contextMenuModel

                delegate: Rectangle {
                    width: parent.width
                    height: Style.defaultRowHeight
                    color: "transparent"

                    H50Text {
                        id: title
                        anchors { horizontalCenter: parent.horizontalCenter }
                        height: parent.height
                        verticalAlignment: Text.AlignVCenter
                        text: model.title
                        color: (listView.currentIndex === index) ? Style.colorTextMarked : Style.colorText
                        font.pointSize: 18

                        onPaintedWidthChanged: {
                            if (paintedWidth > drawer.maxWidth)
                                drawer.maxWidth = paintedWidth
                        }
                    }

                    MouseArea {
                        anchors.fill: parent

                        onClicked: {
                            listView.currentIndex = index
                        }
                    }
                }

                highlight: Rectangle {
                    anchors { left: parent.left; right: parent.right }
                    color: Style.colorMarked
                    focus: true
                }

                onCurrentIndexChanged: {
                    drawer.hide()
                    activeView = currentIndex
                }
            }
        }

        SwipeView {
            id: swipeviewElement
            anchors { top: parent.top; bottom: pageIndicator.top }
            height: Style.displayHeight
            width: 800

            onCurrentIndexChanged: {
                activeView = currentIndex
            }

            Item {
                id: page1

                ListModel{
                    id:listModelPage1

                    ListElement{
                        elementType: "qrc:/Controls/H50DividerAndHeaderDelegate.qml"
                        description: qsTr("Feed forward")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: true
                        minValue: 0
                        maxValue: 3000
                        defaultValue: 2000
                        initOrStorageValue: 0
                        description: qsTr("Crawl speed (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Feeding_Crawl_Speed_Fwd_mA"
                        helpTextBody: qsTr("This is the speed for manual slow speed feed forward .  It can be activated by using Feed Fwd button and the Stop button together.")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: true
                        minValue: 0
                        maxValue: 3000
                        defaultValue: 2000
                        initOrStorageValue: 0
                        description: qsTr("Min speed (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Feeding_FwdMinSpeed_mA"
                        helpTextBody: qsTr("Low speed used by auto feed when the position is close to target.  Set to approximately 25% of max speed.")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: true
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 300
                        initOrStorageValue: 0
                        description: qsTr("High speed (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Feeding_FwdHighSpeed_mA"
                        helpTextBody :qsTr("Set this value to 90-95% of max speed, this is the speed the head will initially drop to when it starts to stop at target.")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible : true
                        minValue: 0
                        maxValue:1000
                        defaultValue:300
                        initOrStorageValue:0
                        description:qsTr("Max speed (%)")
                        dobPath:"Customer.BasAggregat.Configuration_Variables.CV_Feeding_FwdMaxSpeed_mA"
                        helpTextBody:qsTr("Max Speed of feed forward, Set value to a point that the drive wheel will go no faster even if you continue to raise the value in the computer, set at this max value. This sets the max speed that the computer will use. ")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: true
                        minValue: 0
                        maxValue: 400
                        defaultValue: 50
                        initOrStorageValue :0
                        description: qsTr("To photocell speed (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Feeding_ForwardToPhotocell_Speed_mA"
                        helpTextBody: qsTr("Speed at which the head goes to end of log when it is finding end. Used in the \"Find end\" sequence.")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/H50DividerAndHeaderDelegate.qml"
                        description: qsTr("Feed reverse")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: true
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 500
                        initOrStorageValue: 0
                        description: qsTr("Crawl speed (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Feeding_Crawl_Speed_Rev_mA"
                        helpTextBody: qsTr("This is the speed for manual slow speed feed reverse .  It can be activated by using Feed Fwd button and the Stop button together.")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: true
                        minValue: 0
                        maxValue: 400
                        defaultValue: 50
                        initOrStorageValue :0
                        description: qsTr("Min speed (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Feeding_RevMinSpeed_mA"
                        helpTextBody: qsTr("Speed used by auto feed when the head is close to target.  Set to approximately 25% of max speed.")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: true
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 300
                        initOrStorageValue: 0
                        description: qsTr("High speed (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Feeding_RevHighSpeed_mA"
                        helpTextBody :qsTr("Set this value to 90-95% of max speed, this is the speed the head will initially drop to when it starts to stop at target.")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: true
                        minValue: 0
                        maxValue: 400
                        defaultValue: 50
                        initOrStorageValue :0
                        description: qsTr("Max speed (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Feeding_RevMaxSpeed_mA"
                        helpTextBody: qsTr("Max Speed of feed reverse, Set value to a point that the drive wheel will go no faster even if you continue to raise the value in the computer, set at this max value. This sets the max speed that the computer will use. ")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: true
                        minValue: 0
                        maxValue: 400
                        defaultValue: 50
                        initOrStorageValue :0
                        description: qsTr("To photocell speed (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Feeding_ReverseToPhotocell_Speed_mA"
                        helpTextBody: qsTr("Speed at which the head goes to end of log when it is finding end. Used in the \"Find end\" sequence.")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/H50DividerAndHeaderDelegate.qml"
                        description: qsTr("Slope")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 200
                        defaultValue: 0
                        initOrStorageValue: 0
                        description: qsTr("Brake distance to cut window (cm)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_BrakeDistance_cm"
                        helpTextBody: qsTr("Distance from cutting window where the automatic feeding will start to ramp down the speed (cm)")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 5000
                        defaultValue: 2000
                        initOrStorageValue: 0
                        description: qsTr("Min ramp up/down time (ms)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Feeding_MaxSlope"
                        helpTextBody: qsTr("Ramp up/down time on drive motor to allow a soft start /stop of feeding (milliseconds)")
                    }


                }

                ListView {
                    id: listViewPage1
                    anchors.top:parent.top
                    width: parent.width
                    height: parent.height
                    spacing: 25
                    model: listModelPage1
                    clip: true

                    ScrollBar.vertical: ScrollBar { visible: parent.contentHeight > parent.height; width: 5; anchors.right: parent.right; anchors.rightMargin: Style.displayMargin; policy: ScrollBar.AlwaysOn }

                    delegate: Item {
                        id: delegateComponent

                        property var itemModel: model
                        height: childrenRect.height

                        Loader{
                            id:loaderPage1
                            active: activateLoaders
                            source: model.elementType

                            property bool swipeDelegateSwiping
                            property var model:parent.itemModel
                            property var viewModel: headConfigThreeViewModel ? headConfigThreeViewModel : undefined

                            onSwipeDelegateSwipingChanged: {
                                if (swipeDelegateSwiping)
                                    swipeviewElement.interactive = false
                                else
                                    swipeviewElement.interactive = true
                            }
                        }
                    }
                }
            }

            Item {
                id: page2

                ListModel{
                    id:listModelPage2

                    ListElement{
                        elementType: "qrc:/Controls/H50DividerAndHeaderDelegate.qml"
                        description: qsTr("Jamming protection forward")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigOnOffDelegate.qml"
                        description: qsTr("Jamming protection")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Jamming_FwdProt_On"

                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 3000
                        defaultValue: 2000
                        initOrStorageValue: 0
                        description: qsTr("Start sensivity (ms)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Jamming_FwdStartSensitivity_ms"
                        helpTextBody: qsTr("Sets the sensitivity of the head for stalling functionality.  This is how long the head sees no movement on the measuring wheel and is still sending a drive signal to the motors. If this time is reached then the head determines that it is stalled and goes into stalling function, backing up and trying again. This time is used during start of the movement.")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 3000
                        defaultValue: 2000
                        initOrStorageValue: 0
                        description: qsTr("Cruise sensivity (ms)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Jamming_FwdCruiseSensitivity_ms"
                        helpTextBody: qsTr("Sets the sensitivity of the head for stalling functionality.  This is how long the head sees no movement on the measuring wheel and is still sending a drive signal to the motors. If this time is reached then the head determines that it is stalled and goes into stalling function, backing up and trying again. This time is used when log has started to move and could be set lower than the \"start\" sensitivity")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 300
                        initOrStorageValue: 0
                        description: qsTr("Reverse speed (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Jamming_RevSpeed"
                        helpTextBody :qsTr("Sets the speed for the head when it reveses after getting stalled forward.")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible : false
                        minValue: 0
                        maxValue:1000
                        defaultValue:300
                        initOrStorageValue:0
                        description:qsTr("Reverse distance (cm)")
                        dobPath:"Customer.BasAggregat.Configuration_Variables.CV_Jamming_RevDist"
                        helpTextBody:qsTr("How far back the head will reverse if it stalls")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 400
                        defaultValue: 50
                        initOrStorageValue :0
                        description: qsTr("Max reverse time (ms)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Jamming_MaxRevTime_ms"
                        helpTextBody: qsTr("Sets the maximum time the head will try to go reverse during stalling forward")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/H50DividerAndHeaderDelegate.qml"
                        description: qsTr("Jamming protection reverse")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigOnOffDelegate.qml"
                        description: qsTr("Jamming protection")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Jamming_RevProt_On"

                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 3000
                        defaultValue: 2000
                        initOrStorageValue: 0
                        description: qsTr("Start sensivity (ms)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Jamming_RevStartSensitivity_ms"
                        helpTextBody: qsTr("Sets the sensitivity of the head for stalling functionality.  This is how long the head sees no movement on the measuring wheel and is still sending a drive signal to the motors. If this time is reached then the head determines that it is stalled and goes into stalling function, backing up and trying again. This time is used during start of the movement.")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 3000
                        defaultValue: 2000
                        initOrStorageValue: 0
                        description: qsTr("Cruise sensivity (ms)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Jamming_RevCruiseSensitivity_ms"
                        helpTextBody: qsTr("Sets the sensitivity of the head for stalling functionality.  This is how long the head sees no movement on the measuring wheel and is still sending a drive signal to the motors. If this time is reached then the head determines that it is stalled and goes into stalling function, backing up and trying again. This time is used when log has started to move and could be set lower than the \"start\" sensitivity")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 300
                        initOrStorageValue: 0
                        description: qsTr("Forward speed (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Jamming_FwdSpeed"
                        helpTextBody :qsTr("Sets the speed for the head when it goes forward after getting stalled reverse.")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible : false
                        minValue: 0
                        maxValue:1000
                        defaultValue:300
                        initOrStorageValue:0
                        description:qsTr("Forward distance (cm)")
                        dobPath:"Customer.BasAggregat.Configuration_Variables.CV_Jamming_FwdDist"
                        helpTextBody:qsTr("How far the head will go forward if it stalls while reversing")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 400
                        defaultValue: 50
                        initOrStorageValue :0
                        description: qsTr("Max forward time (ms)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Jamming_MaxFwdTime_ms"
                        helpTextBody: qsTr("Sets the maximum time the head will try to go forward during stalling reverse")
                    }


                    ListElement {
                        elementType: "qrc:/Controls/H50DividerAndHeaderDelegate.qml"
                        description: qsTr("General settings")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 200
                        defaultValue: 0
                        initOrStorageValue: 0
                        description: qsTr("Distance feedrollers to buttsaw (cm)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Jamming_DistWheelsToSaw_cm"
                        helpTextBody: qsTr("Distance between feedrollers and buttsaw. The head will not reverse longer than this distance when stalling going forward, to avoid getting the wheels stuck at the end of the log.")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 5000
                        defaultValue: 2000
                        initOrStorageValue: 0
                        description: qsTr("Minimum length difference (cm)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Jamming_MinLengthDiff"
                        helpTextBody: qsTr("Sets the minum length difference that has to be achieved during the currently set sensitivity time for not triggering the jamming protection. Start value 1 cm. Higher value will trigger the jamming proctection more easily")
                    }


                }

                ListView {
                    id: listViewPage2
                    anchors.top:parent.top
                    width: parent.width
                    height: parent.height
                    spacing: 25
                    model: listModelPage2
                    clip: true

                    ScrollBar.vertical: ScrollBar { visible: parent.contentHeight > parent.height; width: 5; anchors.right: parent.right; anchors.rightMargin: Style.displayMargin; policy: ScrollBar.AlwaysOn }

                    delegate: Item {
                        id: delegateComponentPage2

                        property var itemModel: model
                        height: childrenRect.height

                        Loader{
                            id:loaderPage2
                            active: activateLoaders
                            source: model.elementType

                            property bool swipeDelegateSwiping
                            property var model:parent.itemModel
                            property var viewModel: headConfigThreeViewModel ? headConfigThreeViewModel : undefined

                            onSwipeDelegateSwipingChanged: {
                                if (swipeDelegateSwiping)
                                    swipeviewElement.interactive = false
                                else
                                    swipeviewElement.interactive = true
                            }
                        }
                    }
                }
            }
        }

        H50PageIndicator {
            id: pageIndicator
            count: swipeviewElement.count
            currentIndex: swipeviewElement.currentIndex

            anchors.bottom: parent.bottom
            anchors.horizontalCenter: parent.horizontalCenter
        }

        Component.onCompleted: { viewModelHandler.addViewModel(objectName + "Model", root); activateLoaders = true; currentViewPage = contextMenuModel.get(activeView).title; previousObjectName = objectName }
        Component.onDestruction: {
            previousViewPageIndex = activeView

            viewModelHandler.removeViewModel(objectName + "Model")
        }
    }
}
