import QtQuick 2.8
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.1
import MessageHandler 1.0

import "qrc:/Controls"
import "qrc:/Controls/Configuration"

Component {
    Rectangle {
        id: root
        objectName: "HeadConfigSixView"
        width: 800
        height: Style.displayHeight
        color: Style.colorLight

        property int activeView: 0
        property bool activateLoaders: false

        function toggleContextMenu() {
            drawer.toggle()
        }

        QtObject {
            id: inputModel
            objectName: "inputModel"

            property string currentInSignal
            property string currentInSignalPath: "Customer.BasAggregat.Map_Objects.In.*.Active"

            property string currentOutSignal
            property string currentOutSignalPath: "Customer.BasAggregat.Map_Objects.Out.*.Active"


        }

        ListModel {
            id: contextMenuModel

            ListElement { title: qsTr("Head functions") }
            ListElement { title: qsTr("Bucking") }


        }

        Connections {
            target: root

            onActiveViewChanged:  {
                listView.currentIndex = activeView
                swipeviewElement.currentIndex = activeView

                previousViewPageIndex = activeView
                currentViewPage = contextMenuModel.get(activeView).title
            }
        }

        NavigationDrawer {
            id: drawer
            anchors { top: parent.top; bottom: parent.bottom }
            width: maxWidth + 4 * Style.displayMargin > parent.width * 0.25 ? maxWidth + 4 * Style.displayMargin : parent.width * 0.25
            visualParent: parent
            position: Qt.RightEdge
            color: Style.colorLight
            property int maxWidth: 0

            ListView {
                id: listView
                anchors { fill: parent; topMargin: Style.displayMargin * 4 }
                clip: true
                focus: true
                spacing: 10
                model: contextMenuModel

                delegate: Rectangle {
                    width: parent.width
                    height: Style.defaultRowHeight
                    color: "transparent"

                    H50Text {
                        id: title
                        anchors { horizontalCenter: parent.horizontalCenter }
                        height: parent.height
                        verticalAlignment: Text.AlignVCenter
                        text: model.title
                        color: (listView.currentIndex === index) ? Style.colorTextMarked : Style.colorText
                        font.pointSize: 18

                        onPaintedWidthChanged: {
                            if (paintedWidth > drawer.maxWidth)
                                drawer.maxWidth = paintedWidth
                        }
                    }

                    MouseArea {
                        anchors.fill: parent

                        onClicked: {
                            listView.currentIndex = index
                        }
                    }
                }

                highlight: Rectangle {
                    anchors { left: parent.left; right: parent.right }
                    color: Style.colorMarked
                    focus: true
                }

                onCurrentIndexChanged: {
                    drawer.hide()
                    activeView = currentIndex
                }
            }
        }

        SwipeView {
            id: swipeviewElement
            anchors { top: parent.top; bottom: pageIndicator.top }
            height: Style.displayHeight
            width: 800

            onCurrentIndexChanged: {
                activeView = currentIndex
            }

            Item {
                id: page1

                H50Flipable {
                    id: flipable
                    xAxis: 1
                    angle: 180
                    width: parent.width
                    height: parent.height

                    front: Rectangle {
                        width: parent.width
                        height: parent.height
                        color: "transparent"

                        Component.onCompleted: {
                            var offset = 1
                            for (var i = offset; i < signalMapModel1.count; i++) {
                                var signalMap = headConfigSixViewModel.getSignalMap(signalMapModel1.get(i).dobPath, signalMapModel1.get(i).name)
                                if (signalMap) {
                                    signalMapModel1.get(i).inLocked = signalMap.inLocked
                                    signalMapModel1.get(i).outLocked = signalMap.outLocked
                                    signalMapModel1.get(i).shiftLocked = signalMap.shiftLocked
                                    signalMapModel1.get(i).doubleClickLocked = signalMap.doubleClickLocked
                                    signalMapModel1.get(i).doubleClick = signalMap.doubleClick
                                    signalMapModel1.get(i).inSignal = signalMap.inSignal
                                    signalMapModel1.get(i).shiftSignal = signalMap.shiftSignal
                                    signalMapModel1.get(i).outSignal = signalMap.outSignal
                                    signalMapModel1.get(i).shiftVisible = signalMap.shiftVisible
                                    signalMapModel1.get(i).doubleClickVisible = signalMap.doubleClickVisible
                                }
                            }
                        }

                        ListModel {
                            id: signalMapModel1

                            ListElement{
                                elementType: "qrc:/Controls/H50DividerAndHeaderDelegate.qml"
                                description: qsTr("Feeding")
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Feed forward")
                                name: "Feed_Fwd"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Feed reverse")
                                name: "Feed_Rev"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/H50DividerAndHeaderDelegate.qml"
                                description: qsTr("Arms")
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("All open")
                                name: "Head_All_Open"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("All close")
                                name: "Head_All_Close"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Grab arms open")
                                name: "Grab_Arms_Open"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Grab arms close")
                                name: "Grab_Arms_Close"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Grab arms bump")
                                name: "Grab_Arms_Bump"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Drive arms open")
                                name: "Drive_Arms_Open"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Drive arms close")
                                name: "Drive_Arms_Close"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/H50DividerAndHeaderDelegate.qml"
                                description: qsTr("Saws")
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Butt saw")
                                name: "Butt_Saw"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Butt sawbar out")
                                name: "Buttbar_Out"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Top saw")
                                name: "Top_Saw"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Top sawbar out")
                                name: "Topbar_Out"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Saw toggle")
                                name: "Saw_Toggle"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/H50DividerAndHeaderDelegate.qml"
                                description: qsTr("Misc")
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Rotate CW")
                                name: "Rotate_CW"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Rotate CCW")
                                name: "Rotate_CCW"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Heel rack select")
                                name: "Heel_Rack_Select"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Horn")
                                name: "Extra_Horn"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Auto idle")
                                name: "Extra_AutoIdle"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Mute")
                                name: "Extra_Mute"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Wipers")
                                name: "Extra_Wipers"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("OEM BF1")
                                name: "Extra_OEM_BF1"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }


                        }

                        ListView {
                            id: signalMapList
                            anchors { top: parent.top }
                            width: parent.width
                            height: parent.height
                            spacing: 25
                            model: signalMapModel1
                            clip: true

                            ScrollBar.vertical: ScrollBar { visible: parent.contentHeight > parent.height; width: 5; anchors.right: parent.right; anchors.rightMargin: Style.displayMargin; policy: ScrollBar.AlwaysOn }

                            delegate: Rectangle {
                                id: signalMapDelegate
                                height: Style.defaultRowHeight + 6

                                property var itemModel: model

                                Loader {
                                    active: activateLoaders
                                    source: model.elementType

                                    property bool flip
                                    property var currentItemIndex
                                    property bool swipeDelegateSwiping
                                    property bool clearKeyMapping
                                    property bool selected: (signalMapList.currentIndex === index) ? true : false

                                    property int itemIndex: index
                                    property var model: parent.itemModel
                                    property var viewModel: headConfigSixViewModel ? headConfigSixViewModel : undefined

                                    onSwipeDelegateSwipingChanged: {
                                        if (swipeDelegateSwiping)
                                            swipeviewElement.interactive = false
                                        else
                                            swipeviewElement.interactive = true
                                    }

                                    onCurrentItemIndexChanged: {
                                        if (currentItemIndex < 0)
                                            signalMapList.currentIndex = -2
                                        else
                                            signalMapList.currentIndex = currentItemIndex

                                        h50ConfigKeyMappingControl.currentModel = signalMapModel1.get(signalMapList.currentIndex)
                                    }

                                    onFlipChanged: {
                                        h50ConfigKeyMappingControl.refresh()
                                        h50ConfigKeyMappingControl.enableOutput(false)

                                        flipable.flipped = !flipable.flipped
                                    }

                                    onClearKeyMappingChanged: {
                                        h50ConfigKeyMappingControl.refresh()
                                        h50ConfigKeyMappingControl.clearKeyMapping()
                                        signalMapList.currentIndex = -1
                                    }
                                }
                            }
                        }
                    }

                    back: Rectangle {
                        width: parent.width
                        height: parent.height
                        color: "transparent"

                        H50ConfigKeyMappingControl {
                            id: h50ConfigKeyMappingControl

                            property bool flip

                            property var currentModel
                            property var viewModel: headConfigSixViewModel ? headConfigSixViewModel : undefined

                            onFlipChanged: {
                                h50ConfigKeyMappingControl.enableOutput(true)

                                signalMapList.currentIndex = -2

                                flipable.flipped = !flipable.flipped
                            }
                        }
                    }
                }
            }

            Item {
                id: page2

                H50Flipable {
                    id: flipable2
                    xAxis: 1
                    angle: 180
                    width: parent.width
                    height: parent.height

                    front: Rectangle {
                        width: parent.width
                        height: parent.height
                        color: "transparent"

                        Component.onCompleted: {
                            var offset = 1
                            for (var i = offset; i < signalMapModel2.count; i++) {
                                var signalMap = headConfigSixViewModel.getSignalMap(signalMapModel2.get(i).dobPath, signalMapModel2.get(i).name)
                                if (signalMap) {
                                    signalMapModel2.get(i).inLocked = signalMap.inLocked
                                    signalMapModel2.get(i).outLocked = signalMap.outLocked
                                    signalMapModel2.get(i).shiftLocked = signalMap.shiftLocked
                                    signalMapModel2.get(i).doubleClickLocked = signalMap.doubleClickLocked
                                    signalMapModel2.get(i).doubleClick = signalMap.doubleClick
                                    signalMapModel2.get(i).inSignal = signalMap.inSignal
                                    signalMapModel2.get(i).shiftSignal = signalMap.shiftSignal
                                    signalMapModel2.get(i).outSignal = signalMap.outSignal
                                    signalMapModel2.get(i).shiftVisible = signalMap.shiftVisible
                                    signalMapModel2.get(i).doubleClickVisible = signalMap.doubleClickVisible
                                }
                            }
                        }

                        ListModel {
                            id: signalMapModel2

                            ListElement{
                                elementType: "qrc:/Controls/H50DividerAndHeaderDelegate.qml"
                                description: qsTr("Basic controls")
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Start auto bucking")
                                name: "Start"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Stop auto bucking")
                                name: "Stop"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Find end")
                                name: "Find_End"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Reset length")
                                name: "Reset_Length"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Pre delimb mode toggle")
                                name: "predilmb_Mode_Toggle"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("New stem")
                                name: "NewStem"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Grapple mode toggle")
                                name: "Grapple_Mode"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/H50DividerAndHeaderDelegate.qml"
                                description: qsTr("Module control")
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Module plus")
                                name: "Module_Plus"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Module minus")
                                name: "Module_Minus"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Best shorter module")
                                name: "BestShorterModule"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Closest module")
                                name: "ClosestModule"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/H50DividerAndHeaderDelegate.qml"
                                description: qsTr("Tree species")
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Tree species 1")
                                name: "Treespecies_1"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Tree species 2")
                                name: "Treespecies_2"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Tree species 3")
                                name: "Treespecies_3"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Tree species 4")
                                name: "Treespecies_4"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Tree species 5")
                                name: "Treespecies_5"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Tree species 6")
                                name: "Treespecies_6"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Tree species 7")
                                name: "Treespecies_7"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Tree species 8")
                                name: "Treespecies_8"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Tree species 9")
                                name: "Treespecies_9"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Tree species 10")
                                name: "Treespecies_10"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/H50DividerAndHeaderDelegate.qml"
                                description: qsTr("Length selection")
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Length 1")
                                name: "Length_1"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Length 2")
                                name: "Length_2"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Length 3")
                                name: "Length_3"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Length 4")
                                name: "Length_4"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Length 5")
                                name: "Length_5"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Length 6")
                                name: "Length_6"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Length 7")
                                name: "Length_7"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Length 8")
                                name: "Length_8"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Length 9")
                                name: "Length_9"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Length 10")
                                name: "Length_10"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Length 11")
                                name: "Length_11"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Length 12")
                                name: "Length_12"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Length 13")
                                name: "Length_13"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Length 14")
                                name: "Length_14"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Length 15")
                                name: "Length_15"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Length 16")
                                name: "Length_16"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Length 17")
                                name: "Length_17"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Length 18")
                                name: "Length_18"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Length 19")
                                name: "Length_19"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }

                            ListElement{
                                elementType: "qrc:/Controls/Configuration/H50ConfigKeyMappingDelegate.qml"
                                description: qsTr("Length 20")
                                name: "Length_20"
                                dobPath: "Customer.BasAggregat.Map_Objects"
                                shiftVisible: ""; doubleClickVisible: ""; inLocked: "";  outLocked: "";  shiftLocked: "";  doubleClickLocked: ""; doubleClick: ""; inSignal: ""; shiftSignal: ""; outSignal: ""
                            }
                        }

                        ListView {
                            id: signalMapList2
                            anchors { top: parent.top }
                            width: parent.width
                            height: parent.height
                            spacing: 25
                            model: signalMapModel2
                            clip: true

                            ScrollBar.vertical: ScrollBar { visible: parent.contentHeight > parent.height; width: 5; anchors.right: parent.right; anchors.rightMargin: Style.displayMargin; policy: ScrollBar.AlwaysOn }

                            delegate: Rectangle {
                                id: signalMapDelegate3
                                height: Style.defaultRowHeight + 6

                                property var itemModel: model

                                Loader {
                                    active: activateLoaders
                                    source: model.elementType

                                    property bool flip
                                    property var currentItemIndex
                                    property bool swipeDelegateSwiping
                                    property bool clearKeyMapping
                                    property bool selected: (signalMapList2.currentIndex === index) ? true : false

                                    property int itemIndex: index
                                    property var model: parent.itemModel
                                    property var viewModel: headConfigSixViewModel ? headConfigSixViewModel : undefined

                                    onSwipeDelegateSwipingChanged: {
                                        if (swipeDelegateSwiping)
                                            swipeviewElement.interactive = false
                                        else
                                            swipeviewElement.interactive = true
                                    }

                                    onCurrentItemIndexChanged: {
                                        if (currentItemIndex < 0)
                                            signalMapList2.currentIndex = -2
                                        else
                                            signalMapList2.currentIndex = currentItemIndex

                                        h50ConfigKeyMappingControl2.currentModel = signalMapModel2.get(signalMapList2.currentIndex)
                                    }

                                    onFlipChanged: {
                                        h50ConfigKeyMappingControl2.refresh()
                                        h50ConfigKeyMappingControl2.enableOutput(false)

                                        flipable2.flipped = !flipable2.flipped
                                    }

                                    onClearKeyMappingChanged: {
                                        h50ConfigKeyMappingControl2.refresh()
                                        h50ConfigKeyMappingControl2.clearKeyMapping()
                                        signalMapList2.currentIndex = -1
                                    }
                                }
                            }
                        }
                    }

                    back: Rectangle {
                        width: parent.width
                        height: parent.height
                        color: "transparent"

                        H50ConfigKeyMappingControl {
                            id: h50ConfigKeyMappingControl2

                            property bool flip

                            property var currentModel
                            property var viewModel: headConfigSixViewModel ? headConfigSixViewModel : undefined

                            onFlipChanged: {
                                h50ConfigKeyMappingControl2.enableOutput(true)

                                signalMapList2.currentIndex = -1

                                flipable2.flipped = !flipable2.flipped
                            }
                        }
                    }
                }
            }

        }

        H50PageIndicator {
            id: pageIndicator
            count: swipeviewElement.count
            currentIndex: swipeviewElement.currentIndex

            anchors.bottom: parent.bottom
            anchors.horizontalCenter: parent.horizontalCenter
        }

        Component.onCompleted: { viewModelHandler.addViewModel(objectName + "Model", root); activateLoaders = true; currentViewPage = contextMenuModel.get(activeView).title; previousObjectName = objectName }
        Component.onDestruction: {
            previousViewPageIndex = activeView

            if (h50ConfigKeyMappingControl.currentModel)
                h50ConfigKeyMappingControl.enableOutput(true)

            viewModelHandler.removeViewModel(objectName + "Model")
        }
    }
}
