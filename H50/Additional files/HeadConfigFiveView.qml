import QtQuick 2.8
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.1
import MessageHandler 1.0

import "qrc:/Controls"
import "qrc:/Controls/Configuration"

Component {
    Rectangle {
        id: root
        objectName: "HeadConfigFiveView"
        width: 800
        height: Style.displayHeight
        color: Style.colorLight

        property int activeView: 0
        property bool activateLoaders: false
        property string moduleStateString : "Unknown"

        function toggleContextMenu() {
            drawer.toggle()
        }

        QtObject {
            id: inputModel
            objectName: "inputModel"


            //IO12 status
            property int io12_ModuleStatus
            property string io12_ModuleStatusPath: "Customer.d5CanLib.Interface_Variables.HMI_CanManager_State.Value"

            //IO12 seq step
            property int io12_SeqStep
            property string io12_SeqStepPath: "Customer.d5CanLib.Interface_Variables.HMI_CanManager_SeqStep.Value"

            //Set value pin 1-12
            property int io12_Pin_1_Value
            property string io12_Pin_1_ValuePath: "Customer.d5CanLib.Controller.CAN_2.d5Can.d5Can_SetOut_1_4_ID1.Pin_1.Value"

            property int io12_Pin_2_Value
            property string io12_Pin_2_ValuePath: "Customer.d5CanLib.Controller.CAN_2.d5Can.d5Can_SetOut_1_4_ID1.Pin_2.Value"

            property int io12_Pin_3_Value
            property string io12_Pin_3_ValuePath: "Customer.d5CanLib.Controller.CAN_2.d5Can.d5Can_SetOut_1_4_ID1.Pin_3.Value"

            property int io12_Pin_4_Value
            property string io12_Pin_4_ValuePath: "Customer.d5CanLib.Controller.CAN_2.d5Can.d5Can_SetOut_1_4_ID1.Pin_4.Value"

            property int io12_Pin_5_Value
            property string io12_Pin_5_ValuePath: "Customer.d5CanLib.Controller.CAN_2.d5Can.d5Can_SetOut_5_8_ID1.Pin_5.Value"

            property int io12_Pin_6_Value
            property string io12_Pin_6_ValuePath: "Customer.d5CanLib.Controller.CAN_2.d5Can.d5Can_SetOut_5_8_ID1.Pin_6.Value"

            property int io12_Pin_7_Value
            property string io12_Pin_7_ValuePath: "Customer.d5CanLib.Controller.CAN_2.d5Can.d5Can_SetOut_5_8_ID1.Pin_7.Value"

            property int io12_Pin_8_Value
            property string io12_Pin_8_ValuePath: "Customer.d5CanLib.Controller.CAN_2.d5Can.d5Can_SetOut_5_8_ID1.Pin_8.Value"

            property int io12_Pin_9_Value
            property string io12_Pin_9_ValuePath: "Customer.d5CanLib.Controller.CAN_2.d5Can.d5Can_SetOut_9_12_ID1.Pin_9.Value"

            property int io12_Pin_10_Value
            property string io12_Pin_10_ValuePath: "Customer.d5CanLib.Controller.CAN_2.d5Can.d5Can_SetOut_9_12_ID1.Pin_10.Value"

            property int io12_Pin_11_Value
            property string io12_Pin_11_ValuePath: "Customer.d5CanLib.Controller.CAN_2.d5Can.d5Can_SetOut_9_12_ID1.Pin_11.Value"

            property int io12_Pin_12_Value
            property string io12_Pin_12_ValuePath: "Customer.d5CanLib.Controller.CAN_2.d5Can.d5Can_SetOut_9_12_ID1.Pin_12.Value"


            //Actual values pin 1-12
            property int io12_Pin_1_ActValue
            property string io12_Pin_1_ActValuePath: "Customer.d5CanLib.Controller.CAN_2.d5Can.d5Can_Output_Status_1_4_ID1.Current_1.Value"

            property int io12_Pin_2_ActValue
            property string io12_Pin_2_ActValuePath: "Customer.d5CanLib.Controller.CAN_2.d5Can.d5Can_Output_Status_1_4_ID1.Current_2.Value"

            property int io12_Pin_3_ActValue
            property string io12_Pin_3_ActValuePath: "Customer.d5CanLib.Controller.CAN_2.d5Can.d5Can_Output_Status_1_4_ID1.Current_3.Value"

            property int io12_Pin_4_ActValue
            property string io12_Pin_4_ActValuePath: "Customer.d5CanLib.Controller.CAN_2.d5Can.d5Can_Output_Status_1_4_ID1.Current_4.Value"

            property int io12_Pin_5_ActValue
            property string io12_Pin_5_ActValuePath: "Customer.d5CanLib.Controller.CAN_2.d5Can.d5Can_Output_Status_5_8_ID1.Current_5.Value"

            property int io12_Pin_6_ActValue
            property string io12_Pin_6_ActValuePath: "Customer.d5CanLib.Controller.CAN_2.d5Can.d5Can_Output_Status_5_8_ID1.Current_6.Value"

            property int io12_Pin_7_ActValue
            property string io12_Pin_7_ActValuePath: "Customer.d5CanLib.Controller.CAN_2.d5Can.d5Can_Output_Status_5_8_ID1.Current_7.Value"

            property int io12_Pin_8_ActValue
            property string io12_Pin_8_ActValuePath: "Customer.d5CanLib.Controller.CAN_2.d5Can.d5Can_Output_Status_5_8_ID1.Current_8.Value"

            property int io12_Pin_9_ActValue
            property string io12_Pin_9_ActValuePath: "Customer.d5CanLib.Controller.CAN_2.d5Can.d5Can_Output_Status_9_12_ID1.Current_9.Value"

            property int io12_Pin_10_ActValue
            property string io12_Pin_10_ActValuePath: "Customer.d5CanLib.Controller.CAN_2.d5Can.d5Can_Output_Status_9_12_ID1.Current_10.Value"

            property int io12_Pin_11_ActValue
            property string io12_Pin_11_ActValuePath: "Customer.d5CanLib.Controller.CAN_2.d5Can.d5Can_Output_Status_9_12_ID1.Current_11.Value"

            property int io12_Pin_12_ActValue
            property string io12_Pin_12_ActValuePath: "Customer.d5CanLib.Controller.CAN_2.d5Can.d5Can_Output_Status_9_12_ID1.Current_12.Value"

            //Short circuit status pin 1-12
            property int io12_Pin_1_Short
            property string io12_Pin_1_ShortPath: "Customer.d5CanLib.Controller.CAN_2.d5Can.d5Can_Output_Status_1_4_ID1.Short_1.Value"

            property int io12_Pin_2_Short
            property string io12_Pin_2_ShortPath: "Customer.d5CanLib.Controller.CAN_2.d5Can.d5Can_Output_Status_1_4_ID1.Short_2.Value"

            property int io12_Pin_3_Short
            property string io12_Pin_3_ShortPath: "Customer.d5CanLib.Controller.CAN_2.d5Can.d5Can_Output_Status_1_4_ID1.Short_3.Value"

            property int io12_Pin_4_Short
            property string io12_Pin_4_ShortPath: "Customer.d5CanLib.Controller.CAN_2.d5Can.d5Can_Output_Status_1_4_ID1.Short_4.Value"

            property int io12_Pin_5_Short
            property string io12_Pin_5_ShortPath: "Customer.d5CanLib.Controller.CAN_2.d5Can.d5Can_Output_Status_5_8_ID1.Short_5.Value"

            property int io12_Pin_6_Short
            property string io12_Pin_6_ShortPath: "Customer.d5CanLib.Controller.CAN_2.d5Can.d5Can_Output_Status_5_8_ID1.Short_6.Value"

            property int io12_Pin_7_Short
            property string io12_Pin_7_ShortPath: "Customer.d5CanLib.Controller.CAN_2.d5Can.d5Can_Output_Status_5_8_ID1.Short_7.Value"

            property int io12_Pin_8_Short
            property string io12_Pin_8_ShortPath: "Customer.d5CanLib.Controller.CAN_2.d5Can.d5Can_Output_Status_5_8_ID1.Short_8.Value"

            property int io12_Pin_9_Short
            property string io12_Pin_9_ShortPath: "Customer.d5CanLib.Controller.CAN_2.d5Can.d5Can_Output_Status_9_12_ID1.Short_9.Value"

            property int io12_Pin_10_Short
            property string io12_Pin_10_ShortPath: "Customer.d5CanLib.Controller.CAN_2.d5Can.d5Can_Output_Status_9_12_ID1.Short_10.Value"

            property int io12_Pin_11_Short
            property string io12_Pin_11_ShortPath: "Customer.d5CanLib.Controller.CAN_2.d5Can.d5Can_Output_Status_9_12_ID1.Short_11.Value"

            property int io12_Pin_12_Short
            property string io12_Pin_12_ShortPath: "Customer.d5CanLib.Controller.CAN_2.d5Can.d5Can_Output_Status_9_12_ID1.Short_12.Value"

            //Open circuit status pin 1-12
            property int io12_Pin_1_Open
            property string io12_Pin_1_OpenPath: "Customer.d5CanLib.Controller.CAN_2.d5Can.d5Can_Output_Status_1_4_ID1.Open_1.Value"

            property int io12_Pin_2_Open
            property string io12_Pin_2_OpenPath: "Customer.d5CanLib.Controller.CAN_2.d5Can.d5Can_Output_Status_1_4_ID1.Open_2.Value"

            property int io12_Pin_3_Open
            property string io12_Pin_3_OpenPath: "Customer.d5CanLib.Controller.CAN_2.d5Can.d5Can_Output_Status_1_4_ID1.Open_3.Value"

            property int io12_Pin_4_Open
            property string io12_Pin_4_OpenPath: "Customer.d5CanLib.Controller.CAN_2.d5Can.d5Can_Output_Status_1_4_ID1.Open_4.Value"

            property int io12_Pin_5_Open
            property string io12_Pin_5_OpenPath: "Customer.d5CanLib.Controller.CAN_2.d5Can.d5Can_Output_Status_5_8_ID1.Open_5.Value"

            property int io12_Pin_6_Open
            property string io12_Pin_6_OpenPath: "Customer.d5CanLib.Controller.CAN_2.d5Can.d5Can_Output_Status_5_8_ID1.Open_6.Value"

            property int io12_Pin_7_Open
            property string io12_Pin_7_OpenPath: "Customer.d5CanLib.Controller.CAN_2.d5Can.d5Can_Output_Status_5_8_ID1.Open_7.Value"

            property int io12_Pin_8_Open
            property string io12_Pin_8_OpenPath: "Customer.d5CanLib.Controller.CAN_2.d5Can.d5Can_Output_Status_5_8_ID1.Open_8.Value"

            property int io12_Pin_9_Open
            property string io12_Pin_9_OpenPath: "Customer.d5CanLib.Controller.CAN_2.d5Can.d5Can_Output_Status_9_12_ID1.Open_9.Value"

            property int io12_Pin_10_Open
            property string io12_Pin_10_OpenPath: "Customer.d5CanLib.Controller.CAN_2.d5Can.d5Can_Output_Status_9_12_ID1.Open_10.Value"

            property int io12_Pin_11_Open
            property string io12_Pin_11_OpenPath: "Customer.d5CanLib.Controller.CAN_2.d5Can.d5Can_Output_Status_9_12_ID1.Open_11.Value"

            property int io12_Pin_12_Open
            property string io12_Pin_12_OpenPath: "Customer.d5CanLib.Controller.CAN_2.d5Can.d5Can_Output_Status_9_12_ID1.Open_12.Value"


            property string currentInSignal
            property string currentInSignalPath: "Customer.BasAggregat.Map_Objects.In.*.Active"

            property string currentOutSignal
            property string currentOutSignalPath: "Customer.BasAggregat.Map_Objects.Out.*.Active"




            onIo12_ModuleStatusChanged: {
                if (io12_ModuleStatus == 0)
                    root.moduleStateString = "Powered off"

                if (io12_ModuleStatus == 1)
                    root.moduleStateString = "Waiting for boot"

                if (io12_ModuleStatus == 2)
                    root.moduleStateString = "Configuring"

                if (io12_ModuleStatus == 3)
                    root.moduleStateString = "Running ok"

            }
        }



        ListModel {
            id: contextMenuModel

            ListElement { title: qsTr("Rotator") }
            ListElement { title: qsTr("Pump") }
            ListElement { title: qsTr("Bucking") }
            ListElement { title: qsTr("System") }
            ListElement { title: qsTr("Cab module")}
            ListElement { title: qsTr("Manual override") }
        }

        Connections {
            target: root

            onActiveViewChanged:  {
                listView.currentIndex = activeView
                swipeviewElement.currentIndex = activeView

                previousViewPageIndex = activeView
                currentViewPage = contextMenuModel.get(activeView).title
            }
        }

        NavigationDrawer {
            id: drawer
            anchors { top: parent.top; bottom: parent.bottom }
            width: maxWidth + 4 * Style.displayMargin > parent.width * 0.25 ? maxWidth + 4 * Style.displayMargin : parent.width * 0.25
            visualParent: parent
            position: Qt.RightEdge
            color: Style.colorLight
            property int maxWidth: 0

            ListView {
                id: listView
                anchors { fill: parent; topMargin: Style.displayMargin * 4 }
                clip: true
                focus: true
                spacing: 10
                model: contextMenuModel

                delegate: Rectangle {
                    width: parent.width
                    height: Style.defaultRowHeight
                    color: "transparent"

                    H50Text {
                        id: title
                        anchors { horizontalCenter: parent.horizontalCenter }
                        height: parent.height
                        verticalAlignment: Text.AlignVCenter
                        text: model.title
                        color: (listView.currentIndex === index) ? Style.colorTextMarked : Style.colorText
                        font.pointSize: 18

                        onPaintedWidthChanged: {
                            if (paintedWidth > drawer.maxWidth)
                                drawer.maxWidth = paintedWidth
                        }
                    }

                    MouseArea {
                        anchors.fill: parent

                        onClicked: {
                            listView.currentIndex = index
                        }
                    }
                }

                highlight: Rectangle {
                    anchors { left: parent.left; right: parent.right }
                    color: Style.colorMarked
                    focus: true
                }

                onCurrentIndexChanged: {
                    drawer.hide()
                    activeView = currentIndex
                }
            }
        }

        SwipeView {
            id: swipeviewElement
            anchors { top: parent.top; bottom: pageIndicator.top }
            height: Style.displayHeight
            width: 800

            onCurrentIndexChanged: {
                activeView = currentIndex
            }

            Item {
                id: page1

                ListModel {
                    id:listModelPage1

                    ListElement {
                        elementType: "qrc:/Controls/H50DividerAndHeaderDelegate.qml"
                        description: qsTr("Clockwise")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        description: qsTr("Min speed (mA)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Rotator_CW_MinSpeed_mA"
                        helpTextBody: qsTr("")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        description: qsTr("Max speed (mA)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Rotator_CW_MaxSpeed_mA"
                        helpTextBody: qsTr("")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        description: qsTr("Ramp up (ms)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Rotator_CW_RampUp_ms"
                        helpTextBody: qsTr("")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        description: qsTr("Ramp down (ms)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Rotator_CW_RampDown_ms"
                        helpTextBody: qsTr("")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/H50DividerAndHeaderDelegate.qml"
                        description: qsTr("Counter clockwise")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        description: qsTr("Min speed (mA)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Rotator_CCW_MinSpeed_mA"
                        helpTextBody: qsTr("")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        description: qsTr("Max speed (mA)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Rotator_CCW_MaxSpeed_mA"
                        helpTextBody: qsTr("")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        description: qsTr("Ramp up (ms)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Rotator_CCW_RampUp_ms"
                        helpTextBody: qsTr("")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        description: qsTr("Ramp down (ms)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Rotator_CCW_RampDown_ms"
                        helpTextBody: qsTr("")
                    }
                }

                ListView {
                    id: listPage1
                    anchors.top: parent.top
                    anchors.right: parent.right
                    width: parent.width
                    height: parent.height
                    spacing: 25
                    model:listModelPage1

                    clip: true

                    ScrollBar.vertical: ScrollBar { visible: parent.contentHeight > parent.height; width: 5; anchors.right: parent.right; anchors.rightMargin: Style.displayMargin; policy: ScrollBar.AlwaysOn }

                    delegate:Item {
                        id: delegateComponent

                        property var itemModel: model
                        height: childrenRect.height

                        Loader{
                            id: loaderPage1
                            active: activateLoaders
                            source: model.elementType

                            property bool swipeDelegateSwiping
                            property var model: parent.itemModel
                            property var viewModel: headConfigFiveViewModel ? headConfigFiveViewModel : undefined
                            asynchronous: (activeView === 0) ? false : true

                            onSwipeDelegateSwipingChanged: {
                                if (swipeDelegateSwiping)
                                    swipeviewElement.interactive = false
                                else
                                    swipeviewElement.interactive = true
                            }
                        }
                    }
                }
            }

            Item {
                id: page2

                ListModel {
                    id:listModelPage2

                    ListElement{
                        elementType: "qrc:/Controls/H50DividerAndHeaderDelegate.qml"
                        description:qsTr("Pump 1 settings")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        description: qsTr("Feeding (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Pump_1_Feed_Percent"
                        helpTextBody: qsTr("")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        description: qsTr("Butt saw (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Pump_1_Buttsaw_Percent"
                        helpTextBody: qsTr("")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        description: qsTr("Top saw (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Pump_1_Topsaw_Percent"
                        helpTextBody: qsTr("")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        description: qsTr("Rotator (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Pump_1_Rotate_Percent"
                        helpTextBody: qsTr("")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        description: qsTr("Tools (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Pump_1_Tools_Percent"
                        helpTextBody: qsTr("")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        description: qsTr("Delay off (ms)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Pump_1_DelayOff_ms"
                        helpTextBody: qsTr("")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        description: qsTr("Ramp up time (ms) (ms)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Pump_1_RampUp_ms"
                        helpTextBody: qsTr("")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        description: qsTr("Ramp down time (ms)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Pump_1_RampDown_ms"
                        helpTextBody: qsTr("")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        description: qsTr("Min pump output signal (mA)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Pump_1_MinValue_mA"
                        helpTextBody: qsTr("")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        description: qsTr("Max pump output signal")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Pump_1_MaxValue_mA"
                        helpTextBody: qsTr("")
                    }

                    ListElement{
                        elementType: "qrc:/Controls/H50DividerAndHeaderDelegate.qml"
                        description:qsTr("Pump 2 settings")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        description: qsTr("Feeding (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Pump_2_Feed_Percent"
                        helpTextBody: qsTr("")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        description: qsTr("Butt saw (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Pump_2_Buttsaw_Percent"
                        helpTextBody: qsTr("")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        description: qsTr("Top saw (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Pump_2_Topsaw_Percent"
                        helpTextBody: qsTr("")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        description: qsTr("Rotator (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Pump_2_Rotate_Percent"
                        helpTextBody: qsTr("")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        description: qsTr("Tools (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Pump_2_Tools_Percent"
                        helpTextBody: qsTr("")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        description: qsTr("Delay off (ms)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Pump_2_DelayOff_ms"
                        helpTextBody: qsTr("")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        description: qsTr("Ramp up time (ms) (ms)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Pump_2_RampUp_ms"
                        helpTextBody: qsTr("")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        description: qsTr("Ramp down time (ms)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Pump_2_RampDown_ms"
                        helpTextBody: qsTr("")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        description: qsTr("Min pump output signal (mA)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Pump_2_MinValue_mA"
                        helpTextBody: qsTr("")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        description: qsTr("Max pump output signal")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Pump_2_MaxValue_mA"
                        helpTextBody: qsTr("")
                    }
                }

                ListView {
                    id: listPage2
                    anchors { top: parent.top }
                    width: parent.width
                    height: parent.height
                    spacing: 25
                    model: listModelPage2
                    clip: true

                    ScrollBar.vertical: ScrollBar { visible: parent.contentHeight > parent.height; width: 5; anchors.right: parent.right; anchors.rightMargin: Style.displayMargin; policy: ScrollBar.AlwaysOn }

                    delegate: Item {
                        id: delegateComponentPage2

                        property var itemModel: model
                        height: childrenRect.height

                        Loader {
                            id: loaderPage2
                            active: activateLoaders
                            source: model.elementType

                            property bool swipeDelegateSwiping
                            property var model: parent.itemModel
                            property var viewModel: headConfigFiveViewModel ? headConfigFiveViewModel : undefined
                            asynchronous: (activeView === 1) ? false : true

                            onSwipeDelegateSwipingChanged: {
                                if (swipeDelegateSwiping)
                                    swipeviewElement.interactive = false
                                else
                                    swipeviewElement.interactive = true
                            }
                        }
                    }
                }
            }

            Item {
                id: page3

                ListModel {
                    id: listModelPage3

                    ListElement {
                        elementType: "qrc:/Controls/H50DividerAndHeaderDelegate.qml"
                        description: qsTr("Head configuration")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        description: qsTr("Pulses per meter")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Bucking_PulsesPerMeter"
                        helpTextBody: qsTr("Number of pulse the sensor gives per meter, all edges on the pulse signals are counted")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        description: qsTr("Distance diameter sensor to butt saw (cm)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Bucking_SawToDiameterDist"
                        helpTextBody: qsTr("Distance in (cm) between diamater meassuring point and butt saw")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigOnOffDelegate.qml"
                        description: qsTr("Top saw exists")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Topsaw_Exists"
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        description: qsTr("Distance butt saw to top saw (cm)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Bucking_DistanceTopSaw_ButtSaw_cm"
                        helpTextBody: qsTr("")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/H50ComboBoxDelegate.qml"
                        initOrStorageValue: 0
                        description: qsTr("Photocell logic when obstructed")
                        item0: qsTr("No photocell")
                        item1: qsTr("High")
                        item2: qsTr("Low")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Photocell_HighWhenObstructed"
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        description: qsTr("Distance photocell to butt saw (cm)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Bucking_SawToPhotocellDistamce_cm"
                        helpTextBody: qsTr("")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/H50DividerAndHeaderDelegate.qml"
                        description: qsTr("Grapple mode")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigOnOffDelegate.qml"
                        description: qsTr("Enable butt saw")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_System_ButtsawEnabled_GPMode"
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigOnOffDelegate.qml"
                        description: qsTr("Enable top saw")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_System_TopsawEnabled_GPMode"
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigOnOffDelegate.qml"
                        description: qsTr("Enable feeding")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_System_FeedingEnabled_GPMode"
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigOnOffDelegate.qml"
                        description: qsTr("Enable meassure wheel")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_System_MWEnabled_GPMode"
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigOnOffDelegate.qml"
                        description: qsTr("Enable drive arms")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_System_RollerArmsEnabled_GPMode"
                    }

                    ListElement {
                        elementType: "qrc:/Controls/H50DividerAndHeaderDelegate.qml"
                        description: qsTr("Bucking beahaviour")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigOnOffDelegate.qml"
                        description: qsTr("Automatic start after cut")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Bucking_AutoStartAfterCut"
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigOnOffDelegate.qml"
                        description: qsTr("Start from length selection")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Bucking_StartFromLengtSel_OnOff"
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigOnOffDelegate.qml"
                        description: qsTr("Start from find end")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Bucking_StartFromFindEnd"
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigOnOffDelegate.qml"
                        description: qsTr("Hold start for automatic feeding")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Feeding_HoldStartForAutoFeed"
                    }

                    ListElement {
                        elementType: "qrc:/Controls/H50ComboBoxDelegate.qml"
                        initOrStorageValue: 0
                        description: qsTr("Module change behaviour")
                        item0: qsTr("Length order")
                        item1: qsTr("Priority order")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Bucking_ModuleBehaviourType"
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        description: qsTr("Feeding start delay find end (ms)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Bucking_StartDelayFindEnd"
                        helpTextBody: qsTr("Delay the start of Feeding to allow time for the Measuring wheel to come down, when the find end sequence is used and the end of the log is up within the head and the photo cell is not tripped. Set this if needed..")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        description: qsTr("Extended cutting window (cm)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Bucking_ExtendedCutWindow_cm"
                        helpTextBody: qsTr("When length is within this distance from the tagert length no new recalculations are made. This parameter is used to prevent new length selections close to target. Mostly used together with bucking to value priciple.")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/H50ComboBoxDelegate.qml"
                        initOrStorageValue: 0
                        description: qsTr("Action at no more logs")
                        item0: qsTr("Stop")
                        item1: qsTr("Continue")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Bucking_BehaviourAtNoMoreLogs"
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        description: qsTr("Min diameter (mm)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Bucking_MinDiameter"
                        helpTextBody: qsTr("The minimum diameter where automatic feeding will stop. ")
                    }


                }

                ListView {
                    id: listPage3
                    anchors { top: parent.top }
                    width: parent.width
                    height: parent.height
                    spacing: 25
                    model: listModelPage3
                    clip: true

                    ScrollBar.vertical: ScrollBar { visible: parent.contentHeight > parent.height; width: 5; anchors.right: parent.right; anchors.rightMargin: Style.displayMargin; policy: ScrollBar.AlwaysOn }

                    delegate: Item {
                        id: delegateComponentPage3

                        property var itemModel: model
                        height: childrenRect.height

                        Loader {
                            id: loaderPage3
                            active: activateLoaders
                            source: model.elementType

                            property bool swipeDelegateSwiping
                            property var model: parent.itemModel
                            property var viewModel: headConfigFiveViewModel ? headConfigFiveViewModel : undefined
                            asynchronous: (activeView === 2) ? false : true

                            onSwipeDelegateSwipingChanged: {
                                if (swipeDelegateSwiping)
                                    swipeviewElement.interactive = false
                                else
                                    swipeviewElement.interactive = true
                            }
                        }
                    }
                }
            }

            Item {
                id: page4
                ListModel {
                    id: listModelPage4

                    ListElement{
                        elementType: "qrc:/Controls/H50DividerAndHeaderDelegate.qml"
                        description: qsTr("Sound configuration")
                    }

                    ListElement{
                        elementType: "qrc:/Controls/Configuration/H50ConfigOnOffDelegate.qml"
                        description: qsTr("Cut window butt saw")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Bucking_Sound_ButtCutWin_On"
                    }

                    ListElement{
                        elementType: "qrc:/Controls/Configuration/H50ConfigOnOffDelegate.qml"
                        description: qsTr("Cut window top saw")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Bucking_Sound_TopCutWin_On"
                    }

                    ListElement{
                        elementType: "qrc:/Controls/Configuration/H50ConfigOnOffDelegate.qml"
                        description: qsTr("Top saw selected")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Bucking_Sound_TopSawSel_On"
                    }

                    ListElement{
                        elementType: "qrc:/Controls/Configuration/H50ConfigOnOffDelegate.qml"
                        description: qsTr("Stem registred")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Bucking_Sound_StemReg_On"
                    }

                    ListElement{
                        elementType: "qrc:/Controls/Configuration/H50ConfigOnOffDelegate.qml"
                        description: qsTr("Diameter reset")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Bucking_Sound_DiaReset_On"
                    }

                    ListElement{
                        elementType: "qrc:/Controls/Configuration/H50ConfigOnOffDelegate.qml"
                        description: qsTr("Wrong manual selection")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Bucking_Sound_WrongManSel_On"
                    }

                    ListElement{
                        elementType: "qrc:/Controls/Configuration/H50ConfigOnOffDelegate.qml"
                        description: qsTr("Min diameter reached")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Bucking_Sound_MinDiaReached_On"
                    }

                    ListElement{
                        elementType: "qrc:/Controls/Configuration/H50ConfigOnOffDelegate.qml"
                        description: qsTr("Jamming active")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Bucking_Sound_Jamming_On"
                    }

                    ListElement{
                        elementType: "qrc:/Controls/Configuration/H50ConfigOnOffDelegate.qml"
                        description: qsTr("End of tree")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_Bucking_Sound_NoMoreLogs_On"
                    }


                    ListElement{
                        elementType: "qrc:/Controls/H50DividerAndHeaderDelegate.qml"
                        description: qsTr("Pressure sensors")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        description: qsTr("Conversion factor")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_System_PressureConvFactor"
                        helpTextBody: qsTr("Mesassured value in millivolts is multiplied by this factor then divided by 100 to calculate the psi-value.")
                    }
                }

                ListView {
                    id: listPage4
                    anchors { top: parent.top }
                    width: parent.width
                    height: parent.height
                    spacing: 25
                    model: listModelPage4
                    clip: true

                    ScrollBar.vertical: ScrollBar { visible: parent.contentHeight > parent.height; width: 5; anchors.right: parent.right; anchors.rightMargin: Style.displayMargin; policy: ScrollBar.AlwaysOn }

                    delegate: Item {
                        id: delegateComponentPage4

                        property var itemModel: model
                        height: childrenRect.height

                        Loader {
                            id: loaderPage4
                            active: activateLoaders
                            source: model.elementType

                            property bool swipeDelegateSwiping
                            property var model: parent.itemModel
                            property var viewModel: headConfigFiveViewModel ? headConfigFiveViewModel : undefined
                            asynchronous: (activeView === 3) ? false : true

                            onSwipeDelegateSwipingChanged: {
                                if (swipeDelegateSwiping)
                                    swipeviewElement.interactive = false
                                else
                                    swipeviewElement.interactive = true
                            }
                        }
                    }
                }
            }

            Item {
                id: listPage5

                Flickable{
                    width: Style.displayWidth
                    height: Style.displayHeight - 20
                    contentHeight: contentItem.childrenRect.height + 50
                    clip: true

                    H50Text{
                        id: statusHeader
                        text: "Cab module IO12"
                        anchors.left: parent.left
                        anchors.top: parent.top
                        anchors.margins: Style.displayMargin
                        anchors.topMargin: Style.displayMargin * 2
                        bold: true

                    }

                    H50Text{
                        id: moduleStateText
                        text: "Module state :"
                        anchors.left: parent.left
                        anchors.top: statusHeader.bottom
                        anchors.margins: Style.displayMargin
                        anchors.topMargin: Style.displayMargin * 2


                    }

                    H50Text{
                        id: moduleStateValue
                        text: root.moduleStateString
                        anchors.left: moduleStateText.right
                        anchors.top: moduleStateText.top
                        anchors.leftMargin: 20

                    }

                    H50Text{
                        id: moduleSeqText
                        text: "Sequence step :"
                        anchors.left: parent.left
                        anchors.top: moduleStateText.bottom
                        anchors.margins: Style.displayMargin
                        anchors.topMargin: Style.displayMargin * 2


                    }
                    H50Text{
                        id: moduleSeqValue
                        text: inputModel.io12_SeqStep
                        anchors.left: moduleSeqText.right
                        anchors.top: moduleSeqText.top
                        anchors.leftMargin: 20


                    }


                    Row {
                        id: headerRow
                        anchors.top : moduleSeqText.bottom
                        anchors.left : parent.left
                        anchors.leftMargin: Style.displayMargin
                        anchors.topMargin: 10
                        spacing: 0
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*2/20; height: 40; Text {anchors.fill: parent; text: qsTr("Pin"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*5/20; height: 40; Text {anchors.fill: parent; text: qsTr("Name"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*5/20; height: 40; Text {anchors.fill: parent; text: qsTr("Value (mA)"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*5/20; height: 40; Text {anchors.fill: parent; text: qsTr("Act value (mA)"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*2.5/20; height: 40; Text {anchors.fill: parent; text: qsTr("Status"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                    }

                    Row {
                        id: pin1Status
                        anchors.top : headerRow.bottom
                        anchors.left : parent.left
                        anchors.leftMargin: Style.displayMargin
                        spacing: 0
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*2/20; height: 40; Text {anchors.fill: parent; text: qsTr("1"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*5/20; height: 40; Text {anchors.fill: parent; text: qsTr("Rotate CW"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*5/20; height: 40; Text {anchors.fill: parent; text: inputModel.io12_Pin_1_Value; horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*5/20; height: 40; Text {anchors.fill: parent; text: inputModel.io12_Pin_1_ActValue; horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*2.5/20; height: 40;
                            Text {visible: ((inputModel.io12_Pin_1_Short == 0) && inputModel.io12_Pin_1_Open == 0) && (inputModel.io12_ModuleStatus == 3); anchors.fill: parent; text: qsTr("OK"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }
                            Text {visible: (inputModel.io12_Pin_1_Short > 0) && (inputModel.io12_ModuleStatus == 3); anchors.fill: parent; text: qsTr("SHORT"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }
                            Text {visible: (inputModel.io12_Pin_1_Open > 0) && (inputModel.io12_ModuleStatus == 3); anchors.fill: parent; text: qsTr("OPEN"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }
                            Text {visible: (inputModel.io12_ModuleStatus != 3); anchors.fill: parent; text: qsTr("-"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }
                        }
                    }


                    Row {
                        id: pin2Status
                        anchors.top : pin1Status.bottom
                        anchors.left : parent.left
                        anchors.leftMargin: Style.displayMargin
                        spacing: 0
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*2/20; height: 40; Text {anchors.fill: parent; text: qsTr("2"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*5/20; height: 40; Text {anchors.fill: parent; text: qsTr("Rotate CCW"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*5/20; height: 40; Text {anchors.fill: parent; text: inputModel.io12_Pin_2_Value; horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*5/20; height: 40; Text {anchors.fill: parent; text: inputModel.io12_Pin_2_ActValue; horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*2.5/20; height: 40;
                            Text {visible: ((inputModel.io12_Pin_2_Short == 0) && inputModel.io12_Pin_2_Open == 0) && (inputModel.io12_ModuleStatus == 3); anchors.fill: parent; text: qsTr("OK"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }
                            Text {visible: (inputModel.io12_Pin_2_Short > 0) && (inputModel.io12_ModuleStatus == 3); anchors.fill: parent; text: qsTr("SHORT"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }
                            Text {visible: (inputModel.io12_Pin_2_Open > 0) && (inputModel.io12_ModuleStatus == 3); anchors.fill: parent; text: qsTr("OPEN"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }
                            Text {visible: (inputModel.io12_ModuleStatus != 3); anchors.fill: parent; text: qsTr("-"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }
                        }
                    }


                    Row {
                        id: pin3Status
                        anchors.top : pin2Status.bottom
                        anchors.left : parent.left
                        anchors.leftMargin: Style.displayMargin
                        spacing: 0
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*2/20; height: 40; Text {anchors.fill: parent; text: qsTr("3"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*5/20; height: 40; Text {anchors.fill: parent; text: qsTr("Heel rack diverter"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*5/20; height: 40; Text {anchors.fill: parent; text: inputModel.io12_Pin_3_Value; horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*5/20; height: 40; Text {anchors.fill: parent; text: inputModel.io12_Pin_3_ActValue; horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*2.5/20; height: 40;
                            Text {visible: ((inputModel.io12_Pin_3_Short == 0) && inputModel.io12_Pin_3_Open == 0) && (inputModel.io12_ModuleStatus == 3); anchors.fill: parent; text: qsTr("OK"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }
                            Text {visible: (inputModel.io12_Pin_3_Short > 0) && (inputModel.io12_ModuleStatus == 3); anchors.fill: parent; text: qsTr("SHORT"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }
                            Text {visible: (inputModel.io12_Pin_3_Open > 0) && (inputModel.io12_ModuleStatus == 3); anchors.fill: parent; text: qsTr("OPEN"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }
                            Text {visible: (inputModel.io12_ModuleStatus != 3); anchors.fill: parent; text: qsTr("-"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }
                        }
                    }


                    Row {
                        id: pin4Status
                        anchors.top : pin3Status.bottom
                        anchors.left : parent.left
                        anchors.leftMargin: Style.displayMargin
                        spacing: 0
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*2/20; height: 40; Text {anchors.fill: parent; text: qsTr("4"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*5/20; height: 40; Text {anchors.fill: parent; text: qsTr("Pump 1"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*5/20; height: 40; Text {anchors.fill: parent; text: inputModel.io12_Pin_4_Value; horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*5/20; height: 40; Text {anchors.fill: parent; text: inputModel.io12_Pin_4_ActValue; horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*2.5/20; height: 40;
                            Text {visible: ((inputModel.io12_Pin_4_Short == 0) && inputModel.io12_Pin_4_Open == 0) && (inputModel.io12_ModuleStatus == 3); anchors.fill: parent; text: qsTr("OK"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }
                            Text {visible: (inputModel.io12_Pin_4_Short > 0) && (inputModel.io12_ModuleStatus == 3); anchors.fill: parent; text: qsTr("SHORT"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }
                            Text {visible: (inputModel.io12_Pin_4_Open > 0) && (inputModel.io12_ModuleStatus == 3); anchors.fill: parent; text: qsTr("OPEN"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }
                            Text {visible: (inputModel.io12_ModuleStatus != 3); anchors.fill: parent; text: qsTr("-"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }
                        }
                    }


                    Row {
                        id: pin5Status
                        anchors.top : pin4Status.bottom
                        anchors.left : parent.left
                        anchors.leftMargin: Style.displayMargin
                        spacing: 0
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*2/20; height: 40; Text {anchors.fill: parent; text: qsTr("5"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*5/20; height: 40; Text {anchors.fill: parent; text: qsTr("Pump 2"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*5/20; height: 40; Text {anchors.fill: parent; text: inputModel.io12_Pin_5_Value; horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*5/20; height: 40; Text {anchors.fill: parent; text: inputModel.io12_Pin_5_ActValue; horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*2.5/20; height: 40;
                            Text {visible: ((inputModel.io12_Pin_5_Short == 0) && inputModel.io12_Pin_5_Open == 0) && (inputModel.io12_ModuleStatus == 3); anchors.fill: parent; text: qsTr("OK"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }
                            Text {visible: (inputModel.io12_Pin_5_Short > 0) && (inputModel.io12_ModuleStatus == 3); anchors.fill: parent; text: qsTr("SHORT"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }
                            Text {visible: (inputModel.io12_Pin_5_Open > 0) && (inputModel.io12_ModuleStatus == 3); anchors.fill: parent; text: qsTr("OPEN"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }
                            Text {visible: (inputModel.io12_ModuleStatus != 3); anchors.fill: parent; text: qsTr("-"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }
                        }
                    }

                    Row {
                        id: pin6Status
                        anchors.top : pin5Status.bottom
                        anchors.left : parent.left
                        anchors.leftMargin: Style.displayMargin
                        spacing: 0
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*2/20; height: 40; Text {anchors.fill: parent; text: qsTr("6"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*5/20; height: 40; Text {anchors.fill: parent; text: qsTr("Grapple open"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*5/20; height: 40; Text {anchors.fill: parent; text: inputModel.io12_Pin_6_Value; horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*5/20; height: 40; Text {anchors.fill: parent; text: inputModel.io12_Pin_6_ActValue; horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*2.5/20; height: 40;
                            Text {visible: ((inputModel.io12_Pin_6_Short == 0) && inputModel.io12_Pin_6_Open == 0) && (inputModel.io12_ModuleStatus == 3); anchors.fill: parent; text: qsTr("OK"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }
                            Text {visible: (inputModel.io12_Pin_6_Short > 0) && (inputModel.io12_ModuleStatus == 6); anchors.fill: parent; text: qsTr("SHORT"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }
                            Text {visible: (inputModel.io12_Pin_6_Open > 0) && (inputModel.io12_ModuleStatus == 3); anchors.fill: parent; text: qsTr("OPEN"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }
                            Text {visible: (inputModel.io12_ModuleStatus != 3); anchors.fill: parent; text: qsTr("-"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }
                        }
                    }

                    Row {
                        id: pin7Status
                        anchors.top : pin6Status.bottom
                        anchors.left : parent.left
                        anchors.leftMargin: Style.displayMargin
                        spacing: 0
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*2/20; height: 40; Text {anchors.fill: parent; text: qsTr("7"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*5/20; height: 40; Text {anchors.fill: parent; text: qsTr("Grapple close"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*5/20; height: 40; Text {anchors.fill: parent; text: inputModel.io12_Pin_7_Value; horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*5/20; height: 40; Text {anchors.fill: parent; text: inputModel.io12_Pin_7_ActValue; horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*2.5/20; height: 40;
                            Text {visible: ((inputModel.io12_Pin_7_Short == 0) && inputModel.io12_Pin_7_Open == 0) && (inputModel.io12_ModuleStatus == 3); anchors.fill: parent; text: qsTr("OK"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }
                            Text {visible: (inputModel.io12_Pin_7_Short > 0) && (inputModel.io12_ModuleStatus == 3); anchors.fill: parent; text: qsTr("SHORT"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }
                            Text {visible: (inputModel.io12_Pin_7_Open > 0) && (inputModel.io12_ModuleStatus == 3); anchors.fill: parent; text: qsTr("OPEN"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }
                            Text {visible: (inputModel.io12_ModuleStatus != 3); anchors.fill: parent; text: qsTr("-"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }
                        }
                    }

                    Row {
                        id: pin8Status
                        anchors.top : pin7Status.bottom
                        anchors.left : parent.left
                        anchors.leftMargin: Style.displayMargin
                        spacing: 0
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*2/20; height: 40; Text {anchors.fill: parent; text: qsTr("8"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*5/20; height: 40; Text {anchors.fill: parent; text: qsTr("Horn"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*5/20; height: 40; Text {anchors.fill: parent; text: inputModel.io12_Pin_8_Value; horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*5/20; height: 40; Text {anchors.fill: parent; text: inputModel.io12_Pin_8_ActValue; horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*2.5/20; height: 40;
                            Text {visible: ((inputModel.io12_Pin_8_Short == 0) && inputModel.io12_Pin_8_Open == 0) && (inputModel.io12_ModuleStatus == 3); anchors.fill: parent; text: qsTr("OK"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }
                            Text {visible: (inputModel.io12_Pin_8_Short > 0) && (inputModel.io12_ModuleStatus == 3); anchors.fill: parent; text: qsTr("SHORT"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }
                            Text {visible: (inputModel.io12_Pin_8_Open > 0) && (inputModel.io12_ModuleStatus == 3); anchors.fill: parent; text: qsTr("OPEN"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }
                            Text {visible: (inputModel.io12_ModuleStatus != 3); anchors.fill: parent; text: qsTr("-"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }
                        }
                    }

                    Row {
                        id: pin9Status
                        anchors.top : pin8Status.bottom
                        anchors.left : parent.left
                        anchors.leftMargin: Style.displayMargin
                        spacing: 0
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*2/20; height: 40; Text {anchors.fill: parent; text: qsTr("9"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*5/20; height: 40; Text {anchors.fill: parent; text: qsTr("Auto idle"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*5/20; height: 40; Text {anchors.fill: parent; text: inputModel.io12_Pin_9_Value; horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*5/20; height: 40; Text {anchors.fill: parent; text: inputModel.io12_Pin_9_ActValue; horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*2.5/20; height: 40;
                            Text {visible: ((inputModel.io12_Pin_9_Short == 0) && inputModel.io12_Pin_9_Open == 0) && (inputModel.io12_ModuleStatus == 3); anchors.fill: parent; text: qsTr("OK"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }
                            Text {visible: (inputModel.io12_Pin_9_Short > 0) && (inputModel.io12_ModuleStatus == 3); anchors.fill: parent; text: qsTr("SHORT"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }
                            Text {visible: (inputModel.io12_Pin_9_Open > 0) && (inputModel.io12_ModuleStatus == 3); anchors.fill: parent; text: qsTr("OPEN"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }
                            Text {visible: (inputModel.io12_ModuleStatus != 3); anchors.fill: parent; text: qsTr("-"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }
                        }
                    }

                    Row {
                        id: pin10Status
                        anchors.top : pin9Status.bottom
                        anchors.left : parent.left
                        anchors.leftMargin: Style.displayMargin
                        spacing: 0
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*2/20; height: 40; Text {anchors.fill: parent; text: qsTr("10"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*5/20; height: 40; Text {anchors.fill: parent; text: qsTr("Mute"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*5/20; height: 40; Text {anchors.fill: parent; text: inputModel.io12_Pin_10_Value; horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*5/20; height: 40; Text {anchors.fill: parent; text: inputModel.io12_Pin_10_ActValue; horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*2.5/20; height: 40;
                            Text {visible: ((inputModel.io12_Pin_10_Short == 0) && inputModel.io12_Pin_1_Open == 0) && (inputModel.io12_ModuleStatus == 3); anchors.fill: parent; text: qsTr("OK"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }
                            Text {visible: (inputModel.io12_Pin_10_Short > 0) && (inputModel.io12_ModuleStatus == 3); anchors.fill: parent; text: qsTr("SHORT"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }
                            Text {visible: (inputModel.io12_Pin_10_Open > 0) && (inputModel.io12_ModuleStatus == 3); anchors.fill: parent; text: qsTr("OPEN"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }
                            Text {visible: (inputModel.io12_ModuleStatus != 3); anchors.fill: parent; text: qsTr("-"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }
                        }
                    }

                    Row {
                        id: pin11Status
                        anchors.top : pin10Status.bottom
                        anchors.left : parent.left
                        anchors.leftMargin: Style.displayMargin
                        spacing: 0
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*2/20; height: 40; Text {anchors.fill: parent; text: qsTr("11"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*5/20; height: 40; Text {anchors.fill: parent; text: qsTr("Wipers"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*5/20; height: 40; Text {anchors.fill: parent; text: inputModel.io12_Pin_11_Value; horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*5/20; height: 40; Text {anchors.fill: parent; text: inputModel.io12_Pin_11_ActValue; horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*2.5/20; height: 40;
                            Text {visible: ((inputModel.io12_Pin_11_Short == 0) && inputModel.io12_Pin_11_Open == 0) && (inputModel.io12_ModuleStatus == 3); anchors.fill: parent; text: qsTr("OK"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }
                            Text {visible: (inputModel.io12_Pin_11_Short > 0) && (inputModel.io12_ModuleStatus == 3); anchors.fill: parent; text: qsTr("SHORT"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }
                            Text {visible: (inputModel.io12_Pin_11_Open > 0) && (inputModel.io12_ModuleStatus == 3); anchors.fill: parent; text: qsTr("OPEN"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }
                            Text {visible: (inputModel.io12_ModuleStatus != 3); anchors.fill: parent; text: qsTr("-"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }
                        }
                    }

                    Row {
                        id: pin12Status
                        anchors.top : pin11Status.bottom
                        anchors.left : parent.left
                        anchors.leftMargin: Style.displayMargin
                        spacing: 0
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*2/20; height: 40; Text {anchors.fill: parent; text: qsTr("12"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*5/20; height: 40; Text {anchors.fill: parent; text: qsTr("OEM BF1"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*5/20; height: 40; Text {anchors.fill: parent; text: inputModel.io12_Pin_12_Value; horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*5/20; height: 40; Text {anchors.fill: parent; text: inputModel.io12_Pin_12_ActValue; horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }}
                        Rectangle { color: Style.colorDark; border.color: "black"; border.width : 2; width: Style.displayWidth*2.5/20; height: 40;
                            Text {visible: ((inputModel.io12_Pin_12_Short == 0) && inputModel.io12_Pin_12_Open == 0) && (inputModel.io12_ModuleStatus == 3); anchors.fill: parent; text: qsTr("OK"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }
                            Text {visible: (inputModel.io12_Pin_12_Short > 0) && (inputModel.io12_ModuleStatus == 3); anchors.fill: parent; text: qsTr("SHORT"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }
                            Text {visible: (inputModel.io12_Pin_12_Open > 0) && (inputModel.io12_ModuleStatus == 3); anchors.fill: parent; text: qsTr("OPEN"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }
                            Text {visible: (inputModel.io12_ModuleStatus != 3); anchors.fill: parent; text: qsTr("-"); horizontalAlignment: Text.AlignHCenter; verticalAlignment:Text.AlignVCenter }
                        }
                    }
                }
            }
        }

        H50PageIndicator {
            id: pageIndicator
            count: swipeviewElement.count
            currentIndex: swipeviewElement.currentIndex

            anchors.bottom: parent.bottom
            anchors.horizontalCenter: parent.horizontalCenter
        }


        Component.onCompleted: { viewModelHandler.addViewModel(objectName + "Model", root); activateLoaders = true; currentViewPage = contextMenuModel.get(activeView).title; previousObjectName = objectName }
        Component.onDestruction: {
            previousViewPageIndex = activeView;

            viewModelHandler.removeViewModel(objectName + "Model")
        }
    }
}
