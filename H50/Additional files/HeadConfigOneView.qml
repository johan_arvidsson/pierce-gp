import QtQuick 2.8
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.1
import MessageHandler 1.0


import "qrc:/Controls"
import "qrc:/Controls/Configuration"
import "qrc:/Views/Adaptable"

Component {
    Rectangle {
        id: root
        objectName: "HeadConfigOneView"
        width: Style.displayWidth
        height: Style.displayHeight
        color: Style.colorLight

        property int activeView: 0
        property bool activateLoaders: false

        function toggleContextMenu() {
            drawer.toggle()
        }

        QtObject {
            id: inputModel
            objectName: "inputModel"

            property string currentInSignal
            property string currentInSignalPath: "Customer.BasAggregat.Map_Objects.In.*.Active"

            property string currentOutSignal
            property string currentOutSignalPath: "Customer.BasAggregat.Map_Objects.Out.*.Active"

            property int leftSensorRawValue
            property string leftSensorRawValuePath: "Customer.BasAggregat.Interface_Variables.HMI_GrabArms_LeftSensor_RawValue.Value"

            property int leftSensorCalibValue
            property string leftSensorCalibValuePath: "Customer.BasAggregat.Interface_Variables.HMI_GrabArms_LeftSensor_Calibrated_Pos.Value"

            property int rightSensorRawValue
            property string rightSensorRawValuePath: "Customer.BasAggregat.Interface_Variables.HMI_GrabArms_RightSensor_RawValue.Value"

            property int rightSensorCalibValue
            property string rightSensorCalibValuePath: "Customer.BasAggregat.Interface_Variables.HMI_GrabArms_RightSensor_Calibrated_Pos.Value"
        }

        ListModel {
            id: contextMenuModel

            ListElement { title: qsTr("Timer/Speed") }
            ListElement { title: qsTr("Pressure") }
            ListElement { title: qsTr("Position control") }
            ListElement { title: qsTr("Configuration") }

        }

        Connections {
            target: root

            onActiveViewChanged:  {
                listView.currentIndex = activeView
                swipeviewElement.currentIndex = activeView

                previousViewPageIndex = activeView
                currentViewPage = contextMenuModel.get(activeView).title
            }
        }

        NavigationDrawer {
            id: drawer
            anchors { top: parent.top; bottom: parent.bottom }
            width: maxWidth + 4 * Style.displayMargin > parent.width * 0.25 ? maxWidth + 4 * Style.displayMargin : parent.width * 0.25
            visualParent: parent
            position: Qt.RightEdge
            color: Style.colorLight
            property int maxWidth: 0

            ListView {
                id: listView
                anchors { fill: parent; topMargin: Style.displayMargin * 4 }
                clip: true
                focus: true
                spacing: 10
                model: contextMenuModel

                delegate: Rectangle {
                    width: parent.width
                    height: Style.defaultRowHeight
                    color: "transparent"

                    H50Text {
                        id: title
                        anchors { horizontalCenter: parent.horizontalCenter }
                        height: parent.height
                        verticalAlignment: Text.AlignVCenter
                        text: model.title
                        color: (listView.currentIndex === index) ? Style.colorTextMarked : Style.colorText
                        font.pointSize: 18

                        onPaintedWidthChanged: {
                            if (paintedWidth > drawer.maxWidth)
                                drawer.maxWidth = paintedWidth
                        }
                    }

                    MouseArea {
                        anchors.fill: parent

                        onClicked: {
                            listView.currentIndex = index
                        }
                    }
                }

                highlight: Rectangle {
                    anchors { left: parent.left; right: parent.right }
                    color: Style.colorMarked
                    focus: true
                }

                onCurrentIndexChanged: {
                    drawer.hide()
                    activeView = currentIndex
                }
            }
        }

        SwipeView {
            id: swipeviewElement
            anchors { top: parent.top; bottom: pageIndicator.top }
            height: Style.displayHeight
            width: 800

            onCurrentIndexChanged: {
                activeView = currentIndex
            }

            Item {
                id: page1

                ListModel {
                    id:listModelPage1

                    ListElement {
                        elementType: "qrc:/Controls/H50DividerAndHeaderDelegate.qml"
                        description: qsTr("Basic settings")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"

                        onlineButtonVisible : false
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 300
                        initOrStorageValue: 0
                        description: qsTr("Open speed (%)")
                        dobPath:"Customer.BasAggregat.Configuration_Variables.CV_GrabArms_Open_Speed_mA"
                        helpTextBody: qsTr("Sets opening speed of delimbing arms,  increase number to increase delimbing arm close speed, decrease to slow down.")
                    }


                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"

                        onlineButtonVisible : false
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 600
                        description: qsTr("Close speed (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_GrabArms_Close_Speed_mA"
                        helpTextBody: qsTr("Sets closing speed of delimbing arms,  increase number to increase delimbing arm close speed, decrease to slow down.")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"

                        onlineButtonVisible : false
                        minValue: 0
                        maxValue: 1000
                        defaultValue :800
                        description: qsTr("Open time (ms)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_GrabArms_OpenTime_ms"
                        helpTextBody: qsTr("Activated by key All Open, sets time for how long valve is activated when All Open key is pressed. This should be set long enough so that the arms are completely open before the timer ends.")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"

                        onlineButtonVisible : true
                        minValue: 0
                        maxValue: 1000
                        defaultValue :800
                        description: qsTr("Close delay (ms)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_GrabArms_Delay_Close_ms"
                        helpTextBody: qsTr("Delays arms to close when key All Close activates. This should be set to a low value, just long enough for the pump to come on line. If is set too low on a small machine it could stall the engine.")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/H50DividerAndHeaderDelegate.qml"
                        description: qsTr("Bump timers")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"

                        onlineButtonVisible: true
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 300
                        description: qsTr("Open time at start of feed (ms)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_GrabArms_PulseOpen_FeedStart_ms"
                        helpTextBody: qsTr("Timer for how long the auto bump open will stay active on delimb arms at feed start.  This should be set such that the arms bump open just long enough that it allows the head to start moving.")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"

                        onlineButtonVisible: true
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 600
                        description: qsTr("Disabled time at start of feed (ms)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_GrabArms_DisableAfterStartPulseOpening_ms"
                        helpTextBody: qsTr("Timer to set for how long delimb arms will be disabled after the open time has elapsed during start of feed")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"

                        onlineButtonVisible: true
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 800
                        description: qsTr("Open time during feed forward (ms)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_GrabArms_Fwd_PulseOpen_On_Time_ms"
                        helpTextBody: qsTr("Set timer to control how long the auto bump open is active when feed forward is activated.")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"

                        onlineButtonVisible : true
                        minValue: 0
                        maxValue: 1000
                        defaultValue :800
                        description: qsTr("Close time during feed forward (ms)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_GrabArms_Fwd_PulseOpen_Off_Time_ms"
                        helpTextBody: qsTr("Set pause time between auto bumps when feeding forward.")
                    }


                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"

                        onlineButtonVisible : true
                        minValue: 0
                        maxValue: 1000
                        defaultValue :800
                        description: qsTr("Open time during feed reverse (ms)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_GrabArms_Rev_PulseOpen_On_Time_ms"
                        helpTextBody: qsTr("Set timer to control how long the auto bump open is active when feed reverse is activated.")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"

                        onlineButtonVisible : true
                        minValue: 0
                        maxValue: 1000
                        defaultValue :800
                        description: qsTr("Close time during feed reverse (ms)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_GrabArms_Rev_PulseOpen_Off_Time_ms"
                        helpTextBody: qsTr("Set pause time between auto bumps when feeding reverse.")
                    }


                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"

                        onlineButtonVisible : true
                        minValue: 0
                        maxValue: 1000
                        defaultValue :800
                        description: qsTr("Close after bump (ms)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_GrabArms_CloseAfterBump_ms"
                        helpTextBody: qsTr("Set timer to ensure closing after feeding stops. No new Bump or Pulse Open will happen and output Close Delimbing arms will be active during this time.")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/H50DividerAndHeaderDelegate.qml"
                        description: qsTr("Bump configuration")
                    }


                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigOnOffDelegate.qml"
                        description: qsTr("Bump at manual feed forward")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_GrabArms_AutoBumb_ManFwd"

                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigOnOffDelegate.qml"
                        description: qsTr("Bump at automatic feed forward")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_GrabArms_AutoBumb_AutoFwd"

                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigOnOffDelegate.qml"
                        description: qsTr("Bump at manual feed reverse")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_GrabArms_AutoBumb_ManRev"

                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigOnOffDelegate.qml"
                        description: qsTr("Bump at automatic feed reverse")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_GrabArms_AutoBumb_AutoRev"

                    }

                }

                ListView {
                    id: listPage1
                    anchors.top: parent.top
                    anchors.right: parent.right
                    width: parent.width
                    height: parent.height
                    spacing: 25
                    model:listModelPage1
                    clip: true
                    ScrollBar.vertical: ScrollBar { visible: parent.contentHeight > parent.height; width: 5; anchors.right: parent.right; anchors.rightMargin: Style.displayMargin; policy: ScrollBar.AlwaysOn }

                    delegate:Item {
                        id: delegateComponent

                        property var itemModel: model
                        height: childrenRect.height

                        Loader{
                            id: loaderPage1
                            active: activateLoaders

                            property bool swipeDelegateSwiping
                            property var model: parent.itemModel
                            property var viewModel: headConfigOneViewModel ? headConfigOneViewModel : undefined
                            asynchronous: (activeView === 0) ? false : true

                            source: model.elementType

                            onSwipeDelegateSwipingChanged: {
                                if (swipeDelegateSwiping)
                                    swipeviewElement.interactive = false
                                else
                                    swipeviewElement.interactive = true
                            }
                        }
                    }
                }
            }

            Item {
                id: page2

                ListModel {
                    id:listModelPage2

                    ListElement{
                        elementType: "qrc:/Controls/H50DividerAndHeaderDelegate.qml"
                        description:qsTr("Settings")
                    }

                    ListElement{
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"

                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 4000
                        defaultValue: 1500
                        description: qsTr("Pressure at feed reverse (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_GrabArms_Pressure_feedRev_Percent"
                        helpTextBody: qsTr("Set desired pressure for Delimb Arms at feed Rev in % of Delimb Arms Pressure Graph, this is the table modifier for drive reverse for the delimb arm pressure table, this percentage of the table that the delimb arms will use in drive reverse.")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"

                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 600
                        description: qsTr("Dia limit for max pressure during sawing (mm)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_GrabArms_DiaLimitForMaxPressureSawing"
                        helpTextBody: qsTr("Set the minimum diameter for max pressure during sawing.  Max delimb arm pressure will be applied to all cuts made above this diameter.")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigOnOffDelegate.qml"
                        description: qsTr("Close on target")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_GrabArms_CloseOnTarget_On_Off"

                    }

                    ListElement{
                        elementType: "qrc:/Controls/H50DividerAndHeaderDelegate.qml"
                        description:qsTr("Min/Max values")
                    }


                    ListElement{
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"

                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 4000
                        defaultValue: 1500
                        description: qsTr("Max pressure reduction (mA)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_GrabArms_MaxPressure_Reduction_mA"
                        helpTextBody: qsTr("Set the lowest useful pressure to Delimb arms. Set maximum electrical output to match the minimum hydraulic valve pressure. To do this start the output level at a high value and slowly decrease it until the delimbing arms just start to move.  Set the output level to this minimum point and this is the minimum arms pressure setting.")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"

                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 600
                        description: qsTr("Min pressure reduction (mA)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_GrabArms_MinPressure_Reduction_mA"
                        helpTextBody: qsTr("Set the highest useful pressure to Delimb arms. Set minimul electrical output to match the max hydraulic valve pressure.  To do this slowly decrease the output level while watching the pressure on the delimbing arms until the pressure does not increase any more.  Once the pressure stops increasing set the output level to the point of no more pressure increase, this is the max arm pressure.")
                    }

                    ListElement{
                        elementType: "qrc:/Controls/H50DividerAndHeaderDelegate.qml"
                        description:qsTr("Generic diameter dependent pressure (%)")
                    }

                    ListElement{
                        elementType: "qrc:/Controls/H50GraphTable.qml"
                        inputHeader: qsTr("Dia (mm)")
                        outputHeader: qsTr("%")
                        dobPath: "Customer.BasAggregat.Graphs.CV_GrabArms_Generic_Pressure"
                    }

                    ListElement{
                        elementType: "qrc:/Controls/H50DividerAndHeaderDelegate.qml"
                        description:qsTr("Species dependent pressure ajustment (%)")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"

                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 600
                        description: qsTr("Species 1 (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_GrabArms_Species_1_Pressure_Percent"
                        helpTextBody: qsTr("Set the pressure (%) of the genric pressure for this specific tree species")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"

                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 600
                        description: qsTr("Species 2 (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_GrabArms_Species_2_Pressure_Percent"
                        helpTextBody: qsTr("Set the pressure (%) of the genric pressure for this specific tree species")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"

                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 600
                        description: qsTr("Species 3 (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_GrabArms_Species_3_Pressure_Percent"
                        helpTextBody: qsTr("Set the pressure (%) of the genric pressure for this specific tree species")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"

                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 600
                        description: qsTr("Species 4 (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_GrabArms_Species_4_Pressure_Percent"
                        helpTextBody: qsTr("Set the pressure (%) of the genric pressure for this specific tree species")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"

                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 600
                        description: qsTr("Species 5 (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_GrabArms_Species_5_Pressure_Percent"
                        helpTextBody: qsTr("Set the pressure (%) of the genric pressure for this specific tree species")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"

                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 600
                        description: qsTr("Species 6 (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_GrabArms_Species_6_Pressure_Percent"
                        helpTextBody: qsTr("Set the pressure (%) of the genric pressure for this specific tree species")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"

                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 600
                        description: qsTr("Species 7 (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_GrabArms_Species_7_Pressure_Percent"
                        helpTextBody: qsTr("Set the pressure (%) of the genric pressure for this specific tree species")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"

                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 600
                        description: qsTr("Species 8 (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_GrabArms_Species_8_Pressure_Percent"
                        helpTextBody: qsTr("Set the pressure (%) of the genric pressure for this specific tree species")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"

                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 600
                        description: qsTr("Species 9 (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_GrabArms_Species_9_Pressure_Percent"
                        helpTextBody: qsTr("Set the pressure (%) of the genric pressure for this specific tree species")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"

                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 600
                        description: qsTr("Species 10 (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_GrabArms_Species_10_Pressure_Percent"
                        helpTextBody: qsTr("Set the pressure (%) of the genric pressure for this specific tree species")
                    }
                }

                ListView {
                    id: listPage2
                    anchors { top: parent.top }
                    width: parent.width
                    height: parent.height
                    spacing: 25
                    model: listModelPage2
                    clip: true

                    ScrollBar.vertical: ScrollBar { visible: parent.contentHeight > parent.height; width: 5; anchors.right: parent.right; anchors.rightMargin: Style.displayMargin; policy: ScrollBar.AlwaysOn }

                    delegate: Item {
                        id: delegateComponentPage2

                        property var itemModel: model
                        height: childrenRect.height

                        Loader {
                            id: loaderPage2
                            active: activateLoaders

                            property bool swipeDelegateSwiping
                            property var model: parent.itemModel
                            property var viewModel: headConfigOneViewModel ? headConfigOneViewModel : undefined
                            asynchronous: (activeView === 1) ? false : true

                            source: model.elementType

                            onSwipeDelegateSwipingChanged: {
                                if (swipeDelegateSwiping)
                                    swipeviewElement.interactive = false
                                else
                                    swipeviewElement.interactive = true
                            }
                        }
                    }
                }
            }

            Item {
                id: page3

                ListModel {
                    id: listModelPage3

                    ListElement {
                        elementType: "qrc:/Controls/H50DividerAndHeaderDelegate.qml"
                        description: qsTr("Basic settings")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigOnOffDelegate.qml"
                        description: qsTr("Position control")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_GrabArms_PositionCtrl_OnOff"

                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigOnOffDelegate.qml"
                        description: qsTr("Pulse open position controlled")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_GrabArms_PulseOpen_PosCtrl_OnOff"

                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"

                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 600
                        description: qsTr("Adjust speed open (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_GrabArms_PosCtrl_OpenSpeed_mA"
                        helpTextBody: qsTr("Sets the speed of the delimbing arms opening when the position control needs to increase distance. Increasing the number speeds up opening speed and decreasing will slow the opening speed.  Set the number low to slow the opening speed of the arms.  This will prevent the arms from opening too much during a correction.")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"

                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 600
                        description: qsTr("Adjust speed close (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_GrabArms_PosCtrl_CloseSpeed_mA"
                        helpTextBody: qsTr("Sets the speed of the delimbing arms closing when the position control needs to decrease distance. Increasing the number speeds up closing speed and decreasing will slow the closing speed.  Set the number high to maintain close distance to the stem during processing.")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"

                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 600
                        description: qsTr("Tolerance window (0-255)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_GrabArms_DistanceWindow"
                        helpTextBody: qsTr("The distance the drive arms will be allowed to travel before the head makes a correction to adjust the delimbing knife distance.")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"

                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 600
                        description: qsTr("Min diameter for position control (mm)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_GrabArms_PosCtrl_MinDia_mm"
                        helpTextBody: qsTr("Set the diameter for the Position Control to be active.  All diameters above the setting will enable position control and all diameters below will disable.")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigOnOffDelegate.qml"
                        description: qsTr("Positioning before feeding starts")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_GrabArms_PositioningBeforeFeed"

                    }

                    ListElement {
                        elementType: "qrc:/Controls/H50DividerAndHeaderDelegate.qml"
                        description: qsTr("Preferred distances (0-255)")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"

                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 600
                        description: qsTr("Forward")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_GrabArms_PrefDistFwd"
                        helpTextBody: qsTr("Set desired angle position for the feed arms to hold while feeding a stem forward.")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"

                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 600
                        description: qsTr("Reverse")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_GrabArms_PrefDistRev"
                        helpTextBody: qsTr("Set desired angle position for the feed arms to hold while feeding a stem reverse.")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"

                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 600
                        description: qsTr("Pre delimb")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_GrabArms_PrefDist_PreDelimb"
                        helpTextBody: qsTr("Sets the desired angle position for the feed arms to hold while feeding in predelimb mode.")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"

                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 600
                        description: qsTr("Find end")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_GrabArms_PrefDist_FindEnd"
                        helpTextBody: qsTr("Sets the desired angle position for the feed arms to hold when find end is activated.")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"

                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 600
                        description: qsTr("Additional distance at pulse opening")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_GrabArms_AddedDistance_PulseOpen"
                        helpTextBody: qsTr("Setting this number will allow an additional preferred forward distance at the start of feed.  This is only active when the pulse open position control variable is on")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/H50DividerAndHeaderDelegate.qml"
                        description: qsTr("Actual values")
                    }

                    ListElement {
                        elementType: "LabelAndValueDelegate.qml"
                        description: "Left sensor raw value"
                        displayValue : "leftSensorRawValue"
                    }

                    ListElement {
                        elementType: "LabelAndValueDelegate.qml"
                        description: "Left sensor calibrated value"
                        displayValue : "leftSensorCalibValue"
                    }

                    ListElement {
                        elementType: "LabelAndValueDelegate.qml"
                        description: "Right sensor raw value"
                        displayValue: "rightSensorRawValue"
                    }

                    ListElement {
                        elementType: "LabelAndValueDelegate.qml"
                        description: "Right sensor calibrated value"
                        displayValue: "rightSensorCalibValue"
                    }

                    ListElement {
                        elementType: "ButtonDelegate.qml"
                        description: "Recalibrate"
                        dobPath: "Customer.BasAggregat.Interface_Variables.HMI_GrabArms_ForceNewCalib"
                    }

                }

                ListView {
                    id: listPage3
                    anchors { top: parent.top }
                    width: parent.width
                    height: parent.height
                    spacing: 25
                    model: listModelPage3
                    clip: true

                    ScrollBar.vertical: ScrollBar { visible: parent.contentHeight > parent.height; width: 5; anchors.right: parent.right; anchors.rightMargin: Style.displayMargin; policy: ScrollBar.AlwaysOn }

                    delegate: Item {
                        id: delegateComponentPage3

                        property var itemModel: model
                        height: childrenRect.height

                        Loader {
                            id: loaderPage3
                            active: activateLoaders

                            property bool swipeDelegateSwiping
                            property var model: parent.itemModel
                            property var viewModel: headConfigOneViewModel ? headConfigOneViewModel : undefined
                            property int displayValue: 0
                            asynchronous: (activeView === 2) ? false : true

                            source: model.elementType

                            onSwipeDelegateSwipingChanged: {
                                if (swipeDelegateSwiping)
                                    swipeviewElement.interactive = false
                                else
                                    swipeviewElement.interactive = true
                            }

                            onLoaded: {
                                if (model.displayValue){
                                    item.displayValue = Qt.binding(function() {return inputModel[model.displayValue]});
                                }

                            }
                        }
                    }
                }
            }

            Item {
                id: page4

                ListModel {
                    id: listModelPage4

                    ListElement {
                        elementType: "qrc:/Controls/H50DividerAndHeaderDelegate.qml"
                        description: qsTr("Sensor and calibration setup")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigOnOffDelegate.qml"
                        description: qsTr("Left sensor inverted")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_GrabArms_LeftSensor_Inverted"

                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigOnOffDelegate.qml"
                        description: qsTr("Right sensor inverted")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_GrabArms_RightSensor_Inverted"

                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"

                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 600
                        description: qsTr("Min diameter reset position (pulses)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_GrabArms_MinDiaResetPos_Pulses"
                        helpTextBody: qsTr("Set to the highest pulse count when the head is all closed. This value is used to calibrate the feed arm sensors.")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"

                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 600
                        description: qsTr("Drive arms close time during calibration (ms)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_GrabArms_CloseDuringCalibSeq_ms"
                        helpTextBody: qsTr("Set timer to ensure the drive arms fully closed and opened during the calibration startup.")
                    }
                }

                ListView {
                    id: listPage4
                    anchors { top: parent.top }
                    width: parent.width
                    height: parent.height
                    spacing: 25
                    model: listModelPage4
                    clip: true

                    ScrollBar.vertical: ScrollBar { visible: parent.contentHeight > parent.height; width: 5; anchors.right: parent.right; anchors.rightMargin: Style.displayMargin; policy: ScrollBar.AlwaysOn }

                    delegate: Item {
                        id: delegateComponentPage4

                        property var itemModel: model
                        height: childrenRect.height

                        Loader {
                            id: loaderPage4
                            active: activateLoaders

                            property bool swipeDelegateSwiping
                            property var model: parent.itemModel
                            property var viewModel: headConfigOneViewModel ? headConfigOneViewModel : undefined

                            asynchronous: (activeView === 3) ? false : true

                            source: model.elementType

                            onSwipeDelegateSwipingChanged: {
                                if (swipeDelegateSwiping)
                                    swipeviewElement.interactive = false
                                else
                                    swipeviewElement.interactive = true
                            }
                        }
                    }
                }


            }

        }

        H50PageIndicator {
            id: pageIndicator
            count: swipeviewElement.count
            currentIndex: swipeviewElement.currentIndex

            anchors.bottom: parent.bottom
            anchors.horizontalCenter: parent.horizontalCenter
        }


        Component.onCompleted: { viewModelHandler.addViewModel(objectName + "Model", root); activateLoaders = true; currentViewPage = contextMenuModel.get(activeView).title; previousObjectName = objectName }
        Component.onDestruction: {
            previousViewPageIndex = activeView

            viewModelHandler.removeViewModel(objectName + "Model")
        }
    }
}
