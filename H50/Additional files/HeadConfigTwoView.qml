import QtQuick 2.8
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.1
import MessageHandler 1.0



import "qrc:/Controls"
import "qrc:/Controls/Configuration"

Component {


    Rectangle {
        id: root
        objectName: "HeadConfigTwoView"
        width: 800
        height: Style.displayHeight
        color: Style.colorLight

        property int activeView: 0
        property bool activateLoaders: false

        function toggleContextMenu() {
            drawer.toggle()
        }

        QtObject {
            id: inputModel
            objectName: "inputModel"

            property string currentInSignal
            property string currentInSignalPath: "Customer.BasAggregat.Map_Objects.In.*.Active"

            property string currentOutSignal
            property string currentOutSignalPath: "Customer.BasAggregat.Map_Objects.Out.*.Active"

        }

        ListModel {
            id: contextMenuModel

            ListElement { title: qsTr("Timers/speed") }
            ListElement { title: qsTr("Pressure") }

        }

        Connections {
            target: root

            onActiveViewChanged:  {
                listView.currentIndex = activeView
                swipeviewElement.currentIndex = activeView

                previousViewPageIndex = activeView
                currentViewPage = contextMenuModel.get(activeView).title
            }
        }

        NavigationDrawer {
            id: drawer
            anchors { top: parent.top; bottom: parent.bottom }
            width: maxWidth + 4 * Style.displayMargin > parent.width * 0.25 ? maxWidth + 4 * Style.displayMargin : parent.width * 0.25
            visualParent: parent
            position: Qt.RightEdge
            color: Style.colorLight
            property int maxWidth: 0

            ListView {
                id: listView
                anchors { fill: parent; topMargin: Style.displayMargin * 4 }
                clip: true
                focus: true
                spacing: 10
                model: contextMenuModel

                delegate: Rectangle {
                    width: parent.width
                    height: Style.defaultRowHeight
                    color: "transparent"

                    H50Text {
                        id: title
                        anchors { horizontalCenter: parent.horizontalCenter }
                        height: parent.height
                        verticalAlignment: Text.AlignVCenter
                        text: model.title
                        color: (listView.currentIndex === index) ? Style.colorTextMarked : Style.colorText
                        font.pointSize: 18

                        onPaintedWidthChanged: {
                            if (paintedWidth > drawer.maxWidth)
                                drawer.maxWidth = paintedWidth
                        }
                    }

                    MouseArea {
                        anchors.fill: parent

                        onClicked: {
                            listView.currentIndex = index
                        }
                    }
                }

                highlight: Rectangle {
                    anchors { left: parent.left; right: parent.right }
                    color: Style.colorMarked
                    focus: true
                }

                onCurrentIndexChanged: {
                    drawer.hide()
                    activeView = currentIndex
                }
            }
        }

        SwipeView {
            id: swipeviewElement
            anchors { top: parent.top; bottom: pageIndicator.top }
            height: Style.displayHeight
            width: 800

            onCurrentIndexChanged: {
                activeView = currentIndex
            }

            Item {
                id: page1

                ListModel {
                    id: listModelPage1

                    ListElement {
                        elementType: "qrc:/Controls/H50DividerAndHeaderDelegate.qml"
                        description: qsTr("Basic settings")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 300
                        initOrStorageValue: 0
                        description: qsTr("Open speed (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_DriveArms_Open_Speed_mA"
                        helpTextBody: qsTr("Sets Opening speed of drive arms, increase number to increase Drive arm opening speed.")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 300
                        initOrStorageValue: 0
                        description: qsTr("Close speed (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_DriveArms_Close_Speed_mA"
                        helpTextBody: qsTr("Set Closing Speed of drive arms, increase number to increase drive arm closing speed.")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible : false
                        minValue:0
                        maxValue:1000
                        defaultValue:300
                        initOrStorageValue:0
                        description:qsTr("Open time (ms)")
                        dobPath:"Customer.BasAggregat.Configuration_Variables.CV_DriveArms_OpenTime_ms"
                        helpTextBody:qsTr("Activated by key \"All Open\", Set how long valve is activated when \"All Open\" is Activated.  This should be set long enough so that the arms are completely open before timer ends.")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible : false
                        minValue:0
                        maxValue:1000
                        defaultValue:300
                        initOrStorageValue:0
                        description:qsTr("Close delay (ms)")
                        dobPath:"Customer.BasAggregat.Configuration_Variables.CV_DriveArms_Delay_Close_ms"
                        helpTextBody:qsTr("Delay arms to close when key \"All Close\" activates.  Set the delay between delimbing arms close and drive arms close when \"All Closed\" Activated.  This should be so that the delimbing arms close first and bring the log up into the head before the drive arms start to close.")
                    }


                    ListElement {
                        elementType: "qrc:/Controls/H50DividerAndHeaderDelegate.qml"
                        description: qsTr("Bump timers")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigOnOffDelegate.qml"
                        description: qsTr("Bump at feed reverse")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_DriveArms_PulseOpen_FeedReverse_OnOff"

                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 300
                        initOrStorageValue: 0
                        description: qsTr("Open time during feed reverse (ms)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_DriveArms_PulseOpen_On_Time_ms"
                        helpTextBody: qsTr("Set timer to control auto open time at feed Rev.")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 300
                        initOrStorageValue: 0
                        description: qsTr("Close time during feed reverse (ms)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_DriveArms_PulseOpen_Off_Time_ms"
                        helpTextBody: qsTr("Set pause time between auto bumps at feed Rev.")
                    }                    
                }

                ListView {
                    id: listViewPage1
                    anchors.top:parent.top
                    width: parent.width
                    height: parent.height
                    spacing: 25
                    model: listModelPage1
                    clip: true

                    ScrollBar.vertical: ScrollBar { visible: parent.contentHeight > parent.height; width: 5; anchors.right: parent.right; anchors.rightMargin: Style.displayMargin; policy: ScrollBar.AlwaysOn }

                    delegate: Item {
                        id: delegateComponentPage1

                        property var itemModel: model
                        height: childrenRect.height

                        Loader {
                            id: loaderPage1
                            active: activateLoaders
                            source: model.elementType

                            property bool swipeDelegateSwiping
                            property var model: parent.itemModel
                            property var viewModel: headConfigTwoViewModel ? headConfigTwoViewModel : undefined
                            asynchronous: (activeView === 0) ? false : true

                            onSwipeDelegateSwipingChanged: {
                                if (swipeDelegateSwiping)
                                    swipeviewElement.interactive = false
                                else
                                    swipeviewElement.interactive = true
                            }
                        }
                    }
                }
            }

            Item {
                id: page2



                ListModel {
                    id:listModelPage2

                    ListElement {
                        elementType: "qrc:/Controls/H50DividerAndHeaderDelegate.qml"
                        description: qsTr("Settings")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 300
                        initOrStorageValue: 0
                        description: qsTr("Pressure at feed reverse (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_DriveArms_Pressure_feedRev_Percent"
                        helpTextBody: qsTr("Set desired pressure for Drive Arms at feed Rev in % of \"Drive Arms Pressure Graph\"  This is the table modifier for drive reverse for the drive arm pressure table, this percentage of the table that the drive arms will use when feeding reverse.")

                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 300
                        initOrStorageValue: 0
                        description: qsTr("Pressure at start of feeding (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_DriveArms_StartPressure_Percent"
                        helpTextBody: qsTr("Sets the max pressure at feed start for the drive arms.  Depending on setting for diameter limit for start pressure")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 300
                        initOrStorageValue: 0
                        description: qsTr("Pressure at feed start duration (ms)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_DriveArms_StartPressure_time_ms"
                        helpTextBody: qsTr("Set how long the max arm pressure will be active when feed start is activated, this should be set just long enough for the head to get the log moving.")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 300
                        initOrStorageValue: 0
                        description: qsTr("Diameter limit for start pressure (mm)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_DriveArms_DiaLim_HighPressFeedStart_mm"
                        helpTextBody: qsTr("Diameter limit for max pressure at the drive arms during feed start.  All diameters larger than this setting will have max pressure at the drive arms during feed start.  All diameters smaller than this setting will have regular softclamp pressure.")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 300
                        initOrStorageValue: 0
                        description: qsTr("Pressure increase during grab arms bump (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_DriveArms_Pressure_Increase_GrabArms_Bump"
                        helpTextBody: qsTr("Sets the % of pressure increase for the drive arms when the auto bumps of the delimb arms activate.")
                    }


                    ListElement {
                        elementType: "qrc:/Controls/H50DividerAndHeaderDelegate.qml"
                        description: qsTr("Min/Max values")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 300
                        initOrStorageValue: 0
                        description: qsTr("Max pressure reduction (mA)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_DriveArms_MaxPressure_Reduction_mA"
                        helpTextBody: qsTr("Set the lowest useful pressure to Drive arms.  Set maximum electrical output to match the minimum hydraulic valve pressure.  To do this start the output level at approximateley 1000 mA and slowly increase it until the drive arms just start to move.  Set the output level to this point and this is the minimum arms pressure setting.")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"
                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 300
                        initOrStorageValue: 0
                        description: qsTr("Min pressure reduction (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_DriveArms_MinPressure_Reduction_mA"
                        helpTextBody: qsTr("Set the highest useful pressure to Drive arms. Set minimum electrical output to match the max hydraulic valve pressure.  To do this slowly decrease the output level while watching the pressure on the drive arms until the pressure does not increase any more.  Once the pressure stops increasing set the output level to the point of no more pressure increase, this is the max arm pressure.")
                    }

                    ListElement{
                        elementType: "qrc:/Controls/H50DividerAndHeaderDelegate.qml"
                        description:qsTr("Generic diameter dependent pressure (%)")
                    }

                    ListElement{
                        elementType: "qrc:/Controls/H50GraphTable.qml"
                        inputHeader: qsTr("Dia (mm)")
                        outputHeader: qsTr("%")
                        dobPath: "Customer.BasAggregat.Graphs.CV_DriveArms_Generic_Pressure"
                    }

                    ListElement{
                        elementType: "qrc:/Controls/H50DividerAndHeaderDelegate.qml"
                        description:qsTr("Species dependent pressure ajustment (%)")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"

                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 600
                        description: qsTr("Species 1 (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_DriveArms_Species_1_Pressure_Percent"
                        helpTextBody: qsTr("Set the pressure (%) of the genric pressure for this specific tree species")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"

                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 600
                        description: qsTr("Species 2 (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_DriveArms_Species_2_Pressure_Percent"
                        helpTextBody: qsTr("Set the pressure (%) of the genric pressure for this specific tree species")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"

                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 600
                        description: qsTr("Species 3 (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_DriveArms_Species_3_Pressure_Percent"
                        helpTextBody: qsTr("Set the pressure (%) of the genric pressure for this specific tree species")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"

                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 600
                        description: qsTr("Species 4 (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_DriveArms_Species_4_Pressure_Percent"
                        helpTextBody: qsTr("Set the pressure (%) of the genric pressure for this specific tree species")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"

                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 600
                        description: qsTr("Species 5 (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_DriveArms_Species_5_Pressure_Percent"
                        helpTextBody: qsTr("Set the pressure (%) of the genric pressure for this specific tree species")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"

                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 600
                        description: qsTr("Species 6 (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_DriveArms_Species_6_Pressure_Percent"
                        helpTextBody: qsTr("Set the pressure (%) of the genric pressure for this specific tree species")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"

                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 600
                        description: qsTr("Species 7 (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_DriveArms_Species_7_Pressure_Percent"
                        helpTextBody: qsTr("Set the pressure (%) of the genric pressure for this specific tree species")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"

                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 600
                        description: qsTr("Species 8 (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_DriveArms_Species_8_Pressure_Percent"
                        helpTextBody: qsTr("Set the pressure (%) of the genric pressure for this specific tree species")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"

                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 600
                        description: qsTr("Species 9 (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_DriveArms_Species_9_Pressure_Percent"
                        helpTextBody: qsTr("Set the pressure (%) of the genric pressure for this specific tree species")
                    }

                    ListElement {
                        elementType: "qrc:/Controls/Configuration/H50ConfigSliderDelegate.qml"

                        onlineButtonVisible: false
                        minValue: 0
                        maxValue: 1000
                        defaultValue: 600
                        description: qsTr("Species 10 (%)")
                        dobPath: "Customer.BasAggregat.Configuration_Variables.CV_DriveArms_Species_10_Pressure_Percent"
                        helpTextBody: qsTr("Set the pressure (%) of the genric pressure for this specific tree species")
                    }
                }

                ListView {
                    id: listViewPage2
                    anchors.top:parent.top
                    width: parent.width
                    height: parent.height
                    spacing: 25
                    model: listModelPage2
                    clip: true

                    ScrollBar.vertical: ScrollBar {visible: parent.contentHeight > parent.height; width: 5; anchors.right: parent.right; anchors.rightMargin: Style.displayMargin; policy: ScrollBar.AlwaysOn }

                    delegate: Item {
                        id: delegateComponentPage2

                        property var itemModel: model
                        property bool settingEnabled: false
                        height: settingEnabled ? childrenRect.height : -25

                        Loader {
                            id: loaderPage2
                            active: activateLoaders && settingEnabled
                            source: model.elementType

                            property var model: parent.itemModel
                            property bool swipeDelegateSwiping
                            property var viewModel: headConfigTwoViewModel ? headConfigTwoViewModel : undefined

                            onViewModelChanged: {
                                if (typeof(viewModel) == "undefined")
                                    return
                                if ((typeof model.visiblePath != "undefined")){
                                    settingEnabled = viewModel.getStoredValue(model.visiblePath) === "True"
                                    return
                                }
                                if ((typeof model.hiddenPath != "undefined")){
                                    settingEnabled = !(viewModel.getStoredValue(model.hiddenPath) === "True")
                                    return
                                }
                                settingEnabled = true
                            }

                            asynchronous: (activeView === 1) ? false : true

                            onSwipeDelegateSwipingChanged: {
                                if (swipeDelegateSwiping)
                                    swipeviewElement.interactive = false
                                else
                                    swipeviewElement.interactive = true
                            }
                        }
                    }
                }
            }            
        }

        H50PageIndicator {
            id: pageIndicator
            count: swipeviewElement.count
            currentIndex: swipeviewElement.currentIndex

            anchors.bottom: parent.bottom
            anchors.horizontalCenter: parent.horizontalCenter
        }

        Component.onCompleted: { viewModelHandler.addViewModel(objectName + "Model", root); activateLoaders = true; currentViewPage = contextMenuModel.get(activeView).title; previousObjectName = objectName }
        Component.onDestruction: {
            previousViewPageIndex = activeView


            viewModelHandler.removeViewModel(objectName + "Model")
        }
    }
}
