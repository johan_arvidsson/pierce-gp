import QtQuick 2.8
import QtQuick.Window 2.0
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import MessageHandler 1.0

import "qrc:/Controls"

Component {
    Rectangle {
        id: root
        objectName: "BuckingView"
        width: 800
        height: Style.displayHeight
        color: Style.colorLight



        property bool soundInCutWindowEnabled: false
        property bool simplifiedMode: false
        property bool vimekCarrier:false
        property int treeSpeciesClickCount: 0
        property int hmi_System_ForcedUserLevel: 0

        QtObject {
            id: inputModel
            objectName: "inputModel"

            property int bl_FedLength
            property string bl_FedLengthPath: "BuckingLogic.Session.Values.FedLength"

            property int bl_TotalFedLength
            property string bl_TotalFedLengthPath: "BuckingLogic.Session.Values.TotalFedLength"

            property int bl_SelectedLength
            property string bl_SelectedLengthPath: "BuckingLogic.Session.LengthSelection.SelectedLength"

            property string bl_SelectedProduct
            property string bl_SelectedProductPath: "BuckingLogic.Session.LengthSelection.Product"

            property string bl_CurrentTreeSpecies
            property string bl_CurrentTreeSpeciesPath: "BuckingLogic.Session.Values.CurrentTreeSpeciesName"

            property int bl_DiameterAtSawUB
            property string bl_DiameterAtSawUBPath: "BuckingLogic.Session.Values.DiameterAtSawUnderBark"

            property int bl_DiameterAtTopSawUB
            property string bl_DiameterAtTopSawUBPath: "BuckingLogic.Session.Values.DiameterAtTopSawUnderBark"

            property int bl_DiameterAtSensorOB
            property string bl_DiameterAtSensorOBPath: "BuckingLogic.Measurement.Values.Diameter.Position_A.DiameterOnBark"

            property int bl_NextSelectedLength
            property string bl_NextSelectedLengthPath: "BuckingLogic.Session.LengthSelection.Next.SelectedLength"

            property int bl_MinDiaPosOnStem
            property string bl_MinDiaPosOnStemPath: "BuckingLogic.Session.LengthSelection.MinDiameterPositionOnStem"

            property int bl_MinDiaOnStem
            property string bl_MinDiaOnStemPath: "BuckingLogic.Session.LengthSelection.MinDiameterOnStem"

            property bool bl_RegisterLog
            property string bl_RegisterLogPath: "BuckingLogic.Session.RegisterLog"

            property bool bl_RegisterStem
            property string bl_RegisterStemPath: "BuckingLogic.Session.RegisterStem"

            property string bl_SelectedProductColor
            property string bl_SelectedProductColorPath: "BuckingLogic.Session.LengthSelection.DisplayColor"

            property string bl_NextProductColor
            property string bl_NextProductColorPath: "BuckingLogic.Session.LengthSelection.Next.DisplayColor"

            property int hmi_SawIsOut
            property string hmi_SawIsOutPath: "Customer.BasAggregat.Interface_Variables.HMI_Saw_SawISOut.Value"

            property int hmi_TopSawIsOut
            property string hmi_TopSawIsOutPath: "Customer.BasAggregat.Interface_Variables.HMI_Saws_TopSawIsOut.Value"


            property int hmi_CutWindowActive
            property string hmi_CutWindowActivePath: "Customer.BasAggregat.Interface_Variables.HMI_CutWindowActive.Value"

            property int hmi_NumberOfStems
            property string hmi_NumberOfStemsPath: "Customer.BasAggregat.Nonvolatile_Variabels.SYS_NV_NumberOfStems.Value"

            property int hmi_TotalVolume
            property string hmi_TotalVolumePath: "Customer.BasAggregat.Interface_Variables.HMI_Bucking_TotalVolume.Value"

            property int hmi_Productivity
            property string hmi_ProductivityPath: "Customer.BasAggregat.Interface_Variables.HMI_Bucking_VolumePerHour.Value"

            property int hMI_CutType_Automatic
            property string hMI_CutType_AutomaticPath: "Customer.BasAggregat.Interface_Variables.HMI_CutType_Automatic.Value"

            property int hmi_CutType_CutWait
            property string hmi_CutType_CutWaitPath: "Customer.BasAggregat.Interface_Variables.Hmi_CutType_CutWait.Value"

            property int hMI_CutType_Manual
            property string hMI_CutType_ManualPath: "Customer.BasAggregat.Interface_Variables.HMI_CutType_Manual.Value"

            property int hmi_Jamming_Active
            property string hmi_Jamming_ActivePath: "Customer.BasAggregat.Interface_Variables.HMI_Jamming_Active.Value"

            property int hmi_NoMoreLogs
            property string hmi_NoMoreLogsPath: "Customer.BasAggregat.Interface_Variables.HMI_Bucking_NoMoreLogs.Value"

            property int hmi_MinDiaReached
            property string hmi_MinDiaReachedPath: "Customer.BasAggregat.Interface_Variables.HMI_Bucking_MinDiaReached.Value"

            property bool bl_ManualSelectionFailed
            property string bl_ManualSelectionFailedPath: "BuckingLogic.Session.LengthSelection.ManualSelectionFailed"

            property bool hmi_Feeding_WarmUpCmd
            property string hmi_Feeding_WarmUpCmdPath: "Customer.BasAggregat.Interface_Variables.HMI_Feeding_WarmingUp.Value"

            property bool hmi_Feeding_WarmUpIsActive
            property string hmi_Feeding_WarmUpIsActivePath: "Customer.BasAggregat.Interface_Variables.HMI_Feeding_WarmingUp_Active.Value"

            property bool hmi_StumpTreatment_Activate
            property string hmi_StumpTreatment_ActivatePath: "Customer.BasAggregat.Interface_Variables.HMI_StumpTreatment_Activate.Value"

            property bool hmi_StumpTreatmentIsActive
            property string hmi_StumpTreatmentIsActivePath: "Customer.BasAggregat.Interface_Variables.HMI_Out_StumpTreatment_Active.Value"

            property bool hmi_Topsaw_Active
            property string hmi_Topsaw_ActivePath: "Customer.BasAggregat.Interface_Variables.HMI_Saws_TopSaw_Active.Value"

            property bool hmi_Buttsaw_Active
            property string hmi_Buttsaw_ActivePath: "Customer.BasAggregat.Interface_Variables.HMI_Saws_ButtSaw_Active.Value"

            property bool hmi_GrabArms_ShowCalibReq
            property string hmi_GrabArms_ShowCalibReqPath: "Customer.BasAggregat.Interface_Variables.HMI_GrabArms_ShowCalibReq.Value"

            property bool hmi_Photocell_Active
            property string hmi_Photocell_ActivePath: "Customer.BasAggregat.Interface_Variables.HMI_Photocell_Active.Value"

            property bool hmi_MeasureWheel_IsOut
            property string hmi_MeasureWheel_IsOutPath: "Customer.BasAggregat.Interface_Variables.HMI_MeasureWheel_IsOut.Value"

            property bool hmi_Palets_GrappleModeActive
            property string hmi_Palets_GrappleModeActivePath: "Customer.BasAggregat.Interface_Variables.HMI_Palets_GrappleModeActive.Value"

            property bool hmi_Bucking_HeadClosed
            property string hmi_Bucking_HeadClosedPath: "Customer.BasAggregat.Interface_Variables.HMI_Bucking_HeadClosed.Value"

            property bool hmi_Palets_PreDelimModeActive
            property string hmi_Palets_PreDelimModeActivePath: "Customer.BasAggregat.Interface_Variables.HMI_Palets_PreDelimModeActive.Value"

            property bool hmi_Override_IsEnabled
            property string hmi_Override_IsEnabledPath: "Customer.BasAggregat.Interface_Variables.HMI_Override_IsEnabled.Value"

            property int hmi_System_UserLevel
            property string hmi_System_UserLevelPath: "Customer.BasAggregat.Interface_Variables.HMI_System_UserLevel.Value"

            property int hmi_System_MaxJoystickDelay
            property string hmi_System_MaxJoystickDelayPath : "Customer.BasAggregat.Interface_Variables.HMI_System_MaxJoystickDelay_ms.Value"










        }

        Rectangle {
            id: topBackground
            height: parent.height / 2 - 20
            anchors { top: parent.top; left: parent.left; right: parent.right }
            color: Style.colorDark

            PrimarySecondaryIndicator {
                id: lengthDisplay
                height: parent.height * 0.7
                width: parent.width * 0.375
                anchors { top: parent.top; left: parent.left; margins: Style.displayMargin }
                rtl: true
                primaryValue: inputModel.bl_FedLength
                secondaryFirstValue: parseInt(inputModel.bl_SelectedLength) > 0 ? inputModel.bl_SelectedLength : ""
                secondaryNextText: (parseInt(inputModel.bl_SelectedLength) > 0 && !simplifiedMode)  ? inputModel.bl_SelectedProduct : ""
                metricUnit: "cm"
            }

            Rectangle {
                id: emptyLengthDisplay
                height: (parent.height * 0.3) - (Style.displayMargin * 3)
                width: lengthDisplay.width
                anchors { top: lengthDisplay.bottom; left: parent.left; margins: Style.displayMargin }
                color: Style.colorLight

                Image{
                    id: topsawSelectedImage
                    source: "Images/topsaw.png"
                    fillMode: Image.PreserveAspectFit
                    anchors { verticalCenter: parent.verticalCenter; left: parent.left; margins: Style.displayMargin * 4 }
                    height: emptyDiameterDisplay.height * 0.7
                    visible: (inputModel.hmi_Topsaw_Active > 0)
                    mipmap: true

                }

                Text{
                    id: topsawSelectedText
                    anchors.left: topsawSelectedImage.right
                    anchors.leftMargin: 5
                    text: "TOP"
                    height: parent.height
                    verticalAlignment: Text.AlignVCenter
                    visible: (inputModel.hmi_Topsaw_Active > 0)
                }

                Image{
                    id: buttsawSelectedImage
                    source: "Images/buttsaw.png"
                    fillMode: Image.PreserveAspectFit
                    anchors { verticalCenter: parent.verticalCenter; left: parent.left; margins: Style.displayMargin * 4 }
                    height: emptyDiameterDisplay.height * 0.7
                    visible: (inputModel.hmi_Buttsaw_Active > 0)
                    mipmap: true
                }

                Text{
                    id: buttsawSelectedText
                    anchors.left: buttsawSelectedImage.right
                    anchors.leftMargin: 5
                    text: "MAIN"
                    height: parent.height
                    verticalAlignment: Text.AlignVCenter
                    visible: (inputModel.hmi_Buttsaw_Active > 0)
                }

                Image{
                    id: manualCutImage
                    source: "qrc:/Images/manualCut.png"
                    fillMode: Image.PreserveAspectFit
                    anchors { verticalCenter: parent.verticalCenter; right: parent.right; margins: Style.displayMargin * 4 }
                    height: emptyDiameterDisplay.height * 0.7
                    visible: (inputModel.hMI_CutType_Manual > 0)
                    mipmap: true
                }

                Image{
                    id: autoCutImage
                    source: "qrc:/Images/autoCut.png"
                    fillMode: Image.PreserveAspectFit
                    anchors { verticalCenter: parent.verticalCenter; right: parent.right; margins: Style.displayMargin * 4 }
                    height: emptyDiameterDisplay.height * 0.7
                    visible: (inputModel.hMI_CutType_Automatic > 0)
                    mipmap: true
                }

                Image{
                    id: cutWaitImage
                    source: "qrc:/Images/cutWait.png"
                    fillMode: Image.PreserveAspectFit
                    anchors { verticalCenter: parent.verticalCenter; right: parent.right; margins: Style.displayMargin * 4 }
                    height: emptyDiameterDisplay.height * 0.7
                    visible: (inputModel.hmi_CutType_CutWait > 0)
                    mipmap: true
                }
            }

            Rectangle {
                id: emptyMiddleDisplay
                height: parent.height - (Style.displayMargin * 2)
                width: (parent.width * 0.25) - (Style.displayMargin * 4)
                anchors { top: parent.top; left: lengthDisplay.right; margins: Style.displayMargin }
                color: Style.colorLight

                MouseArea{
                    id: accesLevelPressArea
                    anchors.fill: parent
                    onPressed: {
                        root.treeSpeciesClickCount++;
                        if (root.treeSpeciesClickCount > 2){
                            root.treeSpeciesClickCount = 0
                            if (hmi_System_ForcedUserLevel != 5){
                                hmi_System_ForcedUserLevel = 5
                                buckingViewModel.setAttribute("Customer.BasAggregat.Interface_Variables.HMI_System_UserLevel.Value",hmi_System_ForcedUserLevel)
                                return
                            }

                            hmi_System_ForcedUserLevel = 1;
                            buckingViewModel.setAttribute("Customer.BasAggregat.Interface_Variables.HMI_System_UserLevel.Value",hmi_System_ForcedUserLevel)
                        }
                    }
                }

                H50InverseMouseArea{
                    id: accesLevelPressInverseArea
                    anchors.fill: parent
                    onPressed: {
                        root.treeSpeciesClickCount = 0;

                    }
                }

                Item {
                    anchors.centerIn: parent
                    height: childrenRect.height


                    Image {
                        id: treeImage
                        anchors { top: parent.top; horizontalCenter: parent.horizontalCenter; margins: Style.displayMargin }
                        source: "qrc:/Images/Gran.png"
                        fillMode: Image.PreserveAspectFit
                        width: 50
                        visible: !warningImage.visible
                        mipmap: true
                    }
                    H50Text {
                        id: producText
                        anchors { top: treeImage.bottom; horizontalCenter: parent.horizontalCenter; margins: Style.displayMargin }
                        text: inputModel.bl_CurrentTreeSpecies
                        font.pointSize: 14
                        visible: !warningImage.visible
                    }
                    Image {
                        id: cutWindowImage
                        anchors { top: producText.bottom; horizontalCenter: parent.horizontalCenter; margins: Style.displayMargin }
                        source: "qrc:/Images/inCutWindows.png"
                        fillMode: Image.PreserveAspectFit
                        width: emptyMiddleDisplay.width
                        visible: (inputModel.hmi_CutWindowActive > 0) && !wrongManualSelectionImage.visible                       
                    }

                    Image {
                        id: minDiaImage
                        anchors { top: producText.bottom; horizontalCenter: parent.horizontalCenter; margins: Style.displayMargin }
                        source: "qrc:/Images/MinDiaSymbol.png"
                        fillMode: Image.PreserveAspectFit
                        width: emptyMiddleDisplay.width * 0.3
                        visible: (inputModel.hmi_MinDiaReached > 0) && !wrongManualSelectionImage.visible
                        mipmap: true

                    }

                    Image {
                        id: noMorelogsImage
                        anchors { top: producText.bottom; horizontalCenter: parent.horizontalCenter; margins: Style.displayMargin }
                        source: "qrc:/Images/noMoreLogs.png"
                        fillMode: Image.PreserveAspectFit
                        width: emptyMiddleDisplay.width
                        visible: (inputModel.hmi_NoMoreLogs > 0) && !minDiaImage.visible && !wrongManualSelectionImage.visible
                        mipmap: true

                    }

                    Image {
                        id: jammingImage
                        anchors { top: producText.bottom; horizontalCenter: parent.horizontalCenter; margins: Style.displayMargin }
                        source: "qrc:/Images/jamming.png"
                        fillMode: Image.PreserveAspectFit
                        width: emptyMiddleDisplay.width
                        visible: (inputModel.hmi_Jamming_Active > 0)
                        mipmap: true

                    }

                    Image {
                        id: wrongManualSelectionImage
                        anchors { top: producText.bottom; horizontalCenter: parent.horizontalCenter; margins: Style.displayMargin }
                        source: "qrc:/Images/wrongManualSelection.png"
                        fillMode: Image.PreserveAspectFit
                        width: emptyMiddleDisplay.width
                        visible: inputModel.bl_ManualSelectionFailed
                        mipmap: true

                    }
                }
            }

            PrimarySecondaryIndicator {
                id: diameterDisplay
                height: parent.height * 0.7
                width: parent.width * 0.375
                anchors { top: parent.top; right: parent.right; margins: Style.displayMargin }
                primaryValue:  inputModel.hmi_Topsaw_Active ? inputModel.bl_DiameterAtTopSawUB : inputModel.bl_DiameterAtSawUB
                secondaryFirstValue: inputModel.bl_DiameterAtSensorOB
                secondaryNextText: ""//inputModel.bl_MinDiaPosOnStem
                metricUnit: "mm"
            }

            Rectangle {
                id: emptyDiameterDisplay
                height: (parent.height * 0.3) - (Style.displayMargin * 3)
                width: diameterDisplay.width
                anchors { top: diameterDisplay.bottom; right: parent.right; margins: Style.displayMargin }
                color: (inputModel.hmi_SawIsOut > 0) || (inputModel.hmi_TopSawIsOut > 0) ? "red" : Style.colorLight

                Image {
                    id: buttSawImage
                    source: "Images/ButtSawIsOut.png"
                    fillMode: Image.PreserveAspectFit
                    anchors { verticalCenter: parent.verticalCenter; left: parent.left; margins: Style.displayMargin * 4 }
                    height: emptyDiameterDisplay.height * 0.7
                    visible: (inputModel.hmi_SawIsOut > 0)
                    mipmap: true
                }

                Image {
                    id: topSawImage
                    source: "Images/TopSawIsOut.png"
                    fillMode: Image.PreserveAspectFit
                    anchors { verticalCenter: parent.verticalCenter; left: buttSawImage.right; margins: Style.displayMargin * 4 }
                    height: emptyDiameterDisplay.height * 0.7
                    visible: (inputModel.hmi_TopSawIsOut > 0)
                    mipmap: true
                }
            }
        }

        Rectangle {
            id: bottomInfoLine
            anchors { bottom: parent.bottom; left: parent.left; right: parent.right }
            height: 50
            color: Style.colorDark


            Image {
                id: calibReqImage
                source: "Images/calibrateReq.png"
                fillMode: Image.PreserveAspectFit
                height: parent.height * 0.8
                anchors { verticalCenter: parent.verticalCenter; left: parent.left; margins: Style.displayMargin * 4 }
                visible: (inputModel.hmi_GrabArms_ShowCalibReq > 0)
                mipmap: true
            }

            Image {
                id: meassureWheellImage
                source: "Images/meassureWheel.png"
                fillMode: Image.PreserveAspectFit
                height: parent.height * 0.8
                anchors { verticalCenter: parent.verticalCenter; left: calibReqImage.right; margins: Style.displayMargin * 4 }
                visible: (inputModel.hmi_MeasureWheel_IsOut > 0)
                mipmap: true
            }

            Image {
                id: photocellImage
                source: "Images/photocell.png"
                fillMode: Image.PreserveAspectFit
                height: parent.height * 0.8
                anchors { verticalCenter: parent.verticalCenter; left: meassureWheellImage.right; margins: Style.displayMargin * 4 }
                visible: (inputModel.hmi_Photocell_Active > 0)
                mipmap: true
            }

            Image {
                id: grappleImage
                source: "Images/grapple.png"
                fillMode: Image.PreserveAspectFit
                height: parent.height * 0.8
                anchors { verticalCenter: parent.verticalCenter; left: photocellImage.right; margins: Style.displayMargin * 4 }
                visible: (inputModel.hmi_Palets_GrappleModeActive > 0)
                mipmap: true
            }

            Image {
                id: headImage
                source: "Images/head.png"
                fillMode: Image.PreserveAspectFit
                height: parent.height * 0.8
                anchors { verticalCenter: parent.verticalCenter; left: grappleImage.right; margins: Style.displayMargin * 4 }
                visible: (inputModel.hmi_Bucking_HeadClosed > 0)
                mipmap: true
            }

            Image {
                id: predelimbImage
                source: "Images/predelimb.png"
                fillMode: Image.PreserveAspectFit
                height: parent.height * 0.8
                anchors { verticalCenter: parent.verticalCenter; left: headImage.right; margins: Style.displayMargin * 4 }
                visible: (inputModel.hmi_Palets_PreDelimModeActive > 0)
                mipmap: true
            }

            Image {
                id: padlockImage
                source: "Images/padlock.png"
                fillMode: Image.PreserveAspectFit
                height: parent.height * 0.8
                anchors { verticalCenter: parent.verticalCenter; left: predelimbImage.right; margins: Style.displayMargin * 4 }
                visible: (inputModel.hmi_Override_IsEnabled > 0)
                mipmap: true
            }

            Image {
                id: wrenchImage
                source: "Images/wrench.png"
                fillMode: Image.PreserveAspectFit
                height: parent.height * 0.8
                anchors { verticalCenter: parent.verticalCenter; left: padlockImage.right; margins: Style.displayMargin * 4 }
                visible: (inputModel.hmi_System_UserLevel >= 5)
                mipmap: true
            }

            H50Text{
                id: maxJoyDelayValueText
                anchors.verticalCenter: parent.verticalCenter
                anchors.verticalCenterOffset: 5
                anchors.horizontalCenter: maxJoyDelayHeaderText.horizontalCenter
                text: inputModel.hmi_System_MaxJoystickDelay

                MouseArea{
                    anchors.fill : parent
                    onPressed: buckingViewModel.setAttribute("Customer.BasAggregat.Interface_Variables.HMI_System_MaxJoystickDelay_ms.Value",0)
                }
            }

            Text{
                id: maxJoyDelayHeaderText
                anchors.left: wrenchImage.right
                anchors.leftMargin: Style.displayMargin * 4
                anchors.bottom: maxJoyDelayValueText.top
                text: qsTr("Latency (ms)")
                font.pointSize: 8

                MouseArea{
                    anchors.fill : parent
                    onPressed: buckingViewModel.setAttribute("Customer.BasAggregat.Interface_Variables.HMI_System_MaxJoystickDelay_ms.Value",0)
                }
            }

            Image {
                id: pierceImage
                source: "Images/piercelogo.jpg"
                fillMode: Image.PreserveAspectFit
                height: parent.height
                anchors { verticalCenter: parent.verticalCenter; right: parent.right}
                visible: true
                mipmap: true                
            }

        }



        Rectangle {
            id : stemAndLogViewRectangle
            anchors { top: topBackground.bottom; bottom: bottomInfoLine.top; left: parent.left; right: parent.right }
            anchors.topMargin: Style.displayMargin
            anchors.bottomMargin: Style.displayMargin
            color: Style.colorLight
            width: Style.displayWidth
            visible: !inputModel.machineSetup_ClearingHeadActive

            SwipeView {
                id: swipeviewElement
                anchors.fill: parent

                onCurrentIndexChanged: {
                    buckingViewPageIndex = currentIndex
                }

                Item {
                    H50StemViewer {
                        anchors.fill : parent
                        fedLength: inputModel.bl_FedLength
                        currentSelectedLength: inputModel.bl_SelectedLength
                        nextSelectedLength: inputModel.bl_NextSelectedLength
                        logCutRegistred: inputModel.bl_RegisterLog
                        minDiaOnStem: inputModel.bl_MinDiaOnStem
                        minDiaPosOnStem: inputModel.bl_MinDiaPosOnStem
                        totalFedLength: inputModel.bl_TotalFedLength
                        stemRegistred: inputModel.bl_RegisterStem
                        currentProductColor: inputModel.bl_SelectedProductColor
                        nextProductColor: inputModel.bl_NextProductColor
                        viewModel: buckingViewModel ? buckingViewModel : "undefined"
                    }
                }

                Item {
                    H50CutList {
                        id: cutList
                        anchors.fill: parent
                        viewModel: buckingViewModel ? buckingViewModel : "undefined"
                        simpelMode: simplifiedMode;
                    }
                }
            }

            H50PageIndicator {
                id: pageIndicator
                visible: !cutList.hidePageIndicator
                count: swipeviewElement.count
                currentIndex: swipeviewElement.currentIndex
                anchors.bottom: parent.bottom
                anchors.horizontalCenter: parent.horizontalCenter
            }
        }

        Drawer {
            id:drawerToolBox
            width: 80
            height: Style.displayHeight
            edge: Qt.LeftEdge
            dragMargin: 50


            Rectangle{
                id:toolBoxRectamgle
                anchors.fill:parent
                color: Style.colorLight

                anchors.top: parent.top


                Rectangle{
                    id: warmUpRectangle
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.top: parent.top
                    anchors.topMargin: 60
                    color: (inputModel.hmi_Feeding_WarmUpIsActive > 0) ? "red" : Style.colorLight
                    height: 60
                    width: 60
                    Image {
                        id: warmUpImage
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.horizontalCenter: parent.horizontalCenter
                        width:50
                        height:50

                        source: "qrc:/Images/warmUp.png"
                        fillMode: Image.PreserveAspectFit
                        mipmap: true
                    }
                    MouseArea{
                        anchors.fill : parent
                        onPressed: buckingViewModel.setAttribute("Customer.BasAggregat.Interface_Variables.HMI_Feeding_WarmingUp.Value",1)
                        onReleased: buckingViewModel.setAttribute("Customer.BasAggregat.Interface_Variables.HMI_Feeding_WarmingUp.Value",0)
                    }
                }
            }

        }

        Drawer {
            id:drawerTripMeter
            width: headerTripMeter.width + 20
            height: Style.displayHeight
            edge: Qt.RightEdge
            dragMargin: 50







            Rectangle{
                id:tripMeterRectangle
                anchors.fill:parent
                color: Style.colorLight

                anchors.top: parent.top


                H50Text {
                    id: headerTripMeter
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.top: parent.top
                    anchors.topMargin: 20
                    text: qsTr("Volym")
                    font.pointSize: 24
                }

                H50Text {
                    id: volume
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.top: headerTripMeter.bottom
                    anchors.topMargin: 10
                    font.pointSize: 14
                    text: (simplifiedMode ? rootViewModel.currentM3volumeUnit : rootViewModel.currentM3volumeUnit + rootViewModel.currentM3volumeUnitSufix)
                }

                Image {
                    id: classifiedHeader
                    source: "qrc:/Images/Ok.png"
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.top: volume.bottom
                    anchors.topMargin: 30
                    height: 40
                    width: 40
                }

                H50Text {
                    id: classifiedProduktion
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.top: classifiedHeader.bottom
                    anchors.topMargin: 10
                    metricText: rootViewModel ? rootViewModel.tripMeterClassified : ""
                    metricUnit: "m3"
                }

                Image {
                    id: unclassifiedHeader
                    source: "qrc:/Images/Cancel.png"
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.top: classifiedProduktion.bottom
                    anchors.topMargin: 50
                    height: 40
                    width: 40
                }


                H50Text {
                    id: unclassifiedProduktion
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.top: unclassifiedHeader.bottom
                    anchors.topMargin: 10
                    metricText: rootViewModel ? rootViewModel.tripMeterUnclassified : ""
                    metricUnit: "m3"
                }
                H50ImageButton {
                    id:clearTripMeters
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.top: unclassifiedProduktion.bottom
                    anchors.topMargin: 40
                    height: Style.quadraticButtonSide
                    width: Style.quadraticButtonSide
                    imageSource: "qrc:/Images/Trash.png"
                    invertedImageSource: "qrc:/Images/TrashWhite.png"
                    enableBorder: true
                    onClicked: {
                        popup.open()
                    }
                }
            }
        }



        Popup {
            id: popup
            width: 400
            height: 250
            x: (Style.displayWidth / 2) - (width / 2)
            y: (Style.displayHeight / 2) - (height / 2) - (55 / 2)
            focus: true
            modal: true
            closePolicy: modal ? Popup.CloseOnPressOutside : Popup.CloseOnEscape

            RowLayout {
                id: titleRow
                anchors { left: parent.left; right: parent.right; margins: 10 }
                width: parent.width

                H50Text {
                    id: title
                    anchors { margins: 5 }
                    font.pointSize: 16
                    bold: true
                    text: qsTr("Nollställ volymmätare?")
                }

                Rectangle { Layout.fillWidth: true }
            }

            Rectangle {
                id: delimiter1
                anchors { top: titleRow.bottom; left: parent.left; right: parent.right; topMargin: 5 }
                height: 2
                color: "black"
            }

            RowLayout {
                id: question
                anchors { top: delimiter1.bottom; left: parent.left; right: parent.right; margins: 10 }
                width: parent.width

                H50Text {
                    font.pointSize: 14
                    bold: true
                    Layout.preferredWidth: parent.width
                    wrapMode: Text.Wrap
                    text: qsTr("Är du säker på att du vill nollställa volymmätaren? Befintlig produktion påverkas ej.") + "\n\n" + qsTr("Denna operation kan inte ångras.")
                }

                Rectangle { Layout.fillWidth: true }
            }

            Rectangle { Layout.fillHeight: true }

            RowLayout {
                id: buttons
                anchors { bottom: parent.bottom; left: parent.left; right: parent.right; margins: 10 }
                width: parent.width

                Button {
                    font.pointSize: 14
                    text: qsTr("Nej")

                    background: Rectangle {
                        border.width: 1
                        color: parent.pressed ? "#627890" : "#b6c9d8"
                        implicitWidth: 100
                        implicitHeight: 40
                    }

                    onClicked: {
                        popup.close()
                    }
                }

                Rectangle { Layout.fillWidth: true }

                Button {
                    font.pointSize: 14
                    text: qsTr("Ja")

                    background: Rectangle {
                        border.width: 1
                        color: parent.pressed ? "#627890" : "#b6c9d8"
                        implicitWidth: 100
                        implicitHeight: 40
                    }

                    onClicked: {
                        rootViewModel.resetTripMeters()
                        popup.close()
                    }
                }
            }
        }
        Component.onCompleted: {
            viewModelHandler.addViewModel(objectName + "Model", root)

            if (rootViewModel)
                simplifiedMode = rootViewModel.getSimplifiedMode();

            swipeviewElement.currentIndex = buckingViewPageIndex
        }
        Component.onDestruction: {
            viewModelHandler.removeViewModel(objectName + "Model")
        }
    }
}
