﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Windows;
using Aten.AS.Extensions;
using Aten.Extensions;

namespace BuckingOperatingView
{
    [Export("BuckingOperatingView", typeof(IPluginView))]
    public class PluginView : IPluginView, IPageManagerContainer
    {
        [Import]
        public IPageManager PageManager { get; set; }

        BuckingPage buckingPage;
        FrameworkElement IPluginView.View
        {
            get
            {
                if (buckingPage == null)
                {
                    PageManager.ChangeCurrentPage("BuckingPage");

                    buckingPage = new BuckingPage();
                    buckingPage.Loaded += (s, e) => { PageManager.ShowPage("BuckingPage"); };
                }

                return buckingPage;
            }
        }
    }
}
