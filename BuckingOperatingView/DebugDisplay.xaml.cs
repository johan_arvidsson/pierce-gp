﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BuckingOperatingView
{
	/// <summary>
	/// Interaction logic for DebugDisplay.xaml
	/// </summary>
	public partial class DebugDisplay : UserControl
	{
		public DebugDisplay()
		{
			this.InitializeComponent();
		}

		private void DebugDispPressed(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{



            if (DebugList.Visibility == Visibility.Visible)
            {
                DebugList.Visibility = Visibility.Hidden;
                CutList.Visibility = Visibility.Visible;
            }
            
            if (e.ClickCount >= 3)
            {
                
                DebugList.Visibility = Visibility.Visible;
                CutList.Visibility = Visibility.Hidden;
            }  
                
            // TODO: Add event handler implementation here.
		}
	}
}