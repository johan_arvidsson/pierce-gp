﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BuckingOperatingView
{
	/// <summary>
	/// Interaction logic for DiameterDisplay.xaml
	/// </summary>
	public partial class DiameterDisplay : UserControl
	{
		public DiameterDisplay()
		{
			this.InitializeComponent();
		}



        public string TestString
        {
            get { return (string)GetValue(TestStringProperty); }
            set { SetValue(TestStringProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TestString.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TestStringProperty =
            DependencyProperty.Register("TestString", typeof(string), typeof(DiameterDisplay), new PropertyMetadata(""));

        
	}
}