﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BuckingOperatingView
{
	/// <summary>
	/// Interaction logic for BuckingIndicatorsDisplay.xaml
	/// </summary>
	public partial class BuckingIndicatorsDisplay : UserControl
	{
        MyConfiguration.BasAggregat_Proxy dob = new MyConfiguration.BasAggregat_Proxy();

        public BuckingIndicatorsDisplay()
		{
			this.InitializeComponent();
		}

        private void PadLockMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
                dob.BasAggregat.Interface_Variables.HMI_Override_IsEnabled.Value = 0;
        }

       
    }
}