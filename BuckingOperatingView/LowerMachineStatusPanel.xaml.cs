﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BuckingOperatingView
{
	/// <summary>
	/// Interaction logic for LowerMachineStatusPanel.xaml
	/// </summary>
	public partial class LowerMachineStatusPanel : UserControl
	{
        MyConfiguration.BasAggregat_Proxy dob = new MyConfiguration.BasAggregat_Proxy();

        public LowerMachineStatusPanel()
		{
            
            this.InitializeComponent();
		}

        private void pierceLogoPressed(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount >= 3)
            {
                dob.BasAggregat.Interface_Variables.HMI_System_UserLevel.Value = 5;
            }

            if (e.ClickCount == 1)
            {
                dob.BasAggregat.Interface_Variables.HMI_System_UserLevel.Value = 1;
            }
        }
    }
}