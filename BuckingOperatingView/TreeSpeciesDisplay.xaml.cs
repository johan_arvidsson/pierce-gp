﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BuckingOperatingView
{
	/// <summary>
	/// Interaction logic for TreeSpeciesDisplay.xaml
	/// </summary>
	public partial class TreeSpeciesDisplay : UserControl
	{
        MyConfiguration.BasAggregat_Proxy dobReader = new MyConfiguration.BasAggregat_Proxy();

        public TreeSpeciesDisplay()
		{
			this.InitializeComponent();
            
            
            
            //TreeSpeciesName.GetBindingExpression(TextBlock.TextProperty).UpdateTarget();
        }

        private void Test_MouseEnter(object sender, MouseEventArgs e)
        {
            //Test.Text = System.Convert.ToString(dobReader.BasAggregat.Interface_Variables.HMI_Bucking_SelectedTreeSpecies.Value);
            TreeSpeciesName.GetBindingExpression(TextBlock.TextProperty).UpdateSource();
        }
    }
}