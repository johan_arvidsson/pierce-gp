﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Windows;
using Aten.Extensions;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Reflection;
using Aten.Extensions.Keyboard;
using Aten.AS.Extensions;
using Aten.AS.Visualization.Designer;

namespace DasaTestApplication
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private CompositionContainer _container;

        [Export(typeof(IPageManager))]
        IPageManager PageManager { get { return new PageManager(); } }

        public KeyboardDialogFactoryStub keyboardDialogFactory;
        [Export(typeof(IKeyboardDialogFactory))]
        public KeyboardDialogFactoryStub KeyboardDialogFactory { get { return keyboardDialogFactory ?? (keyboardDialogFactory= new KeyboardDialogFactoryStub()); } }

        [Import("BuckingOperatingView", typeof(IPluginView))]
        public IPluginView PluginView { get; set; }

        [Import(typeof(IPluginMenuView))]
        public IPluginMenuView PluginMenuView { get; set; }

        public App()
        {
            //FPS.VirtualKeyboard.Common.LicenseValidator.LicenceKey = "E2MvwBDIwSLiyrVEhISqBAHDaPV83bpyOg2iP/Jn1Ge0Q5ZO++ATNg==";

            var catalog = new AggregateCatalog();
            catalog.Catalogs.Add(new AssemblyCatalog(Assembly.GetExecutingAssembly()));
            catalog.Catalogs.Add(new DirectoryCatalog(AppDomain.CurrentDomain.BaseDirectory));
            //catalog.Catalogs.Add(new AssemblyCatalog(typeof(Aten.Common.KeyboardDialog).Assembly));

            _container = new CompositionContainer(catalog);
            _container.ComposeParts(this);
        }
    }
}