﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Aten.AS.Visualization.Designer;

namespace DasaTestApplication
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
        private App App { get { return (App)Application.Current; } }

		public MainWindow()
		{
			this.InitializeComponent();

            App.KeyboardDialogFactory.Owner = this;

            HolderC.Content = App.PluginView.View;
            HolderC.Margin = new Thickness(0);
		}

        private void BuckingPageB_Click(object sender, RoutedEventArgs e)
        {
            HolderC.Content = App.PluginView.View;
            HolderC.Margin = new Thickness(0);
        }

        private void ConfigurationPage1B_Click(object sender, RoutedEventArgs e)
        {
            ConfigurationPage(0);
        }

        private void ConfigurationPage2B_Click(object sender, RoutedEventArgs e)
        {
            ConfigurationPage(1);
        }

        private void ConfigurationPage3B_Click(object sender, RoutedEventArgs e)
        {
            ConfigurationPage(2);
        }

        private void ConfigurationPage4B_Click(object sender, RoutedEventArgs e)
        {
            ConfigurationPage(3);
        }

        private void ConfigurationPage5B_Click(object sender, RoutedEventArgs e)
        {
            ConfigurationPage(4);
        }

        private void ConfigurationPage6B_Click(object sender, RoutedEventArgs e)
        {
            ConfigurationPage(5);
        }

        private void ConfigurationPage(int page)
        {
            HolderC.Content = App.PluginMenuView.View;
            HolderC.Margin = new Thickness(10, 0, 10, 0);
            
            App.PluginMenuView.Navigate(App.PluginMenuView.Uris[page]);
        }
	}
}