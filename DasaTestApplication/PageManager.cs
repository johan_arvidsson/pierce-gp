﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aten.AS.Extensions;
using Aten.AS.Extensions.Proxy;

namespace DasaTestApplication
{
    public class PageManager : IPageManager 
    {
        public PageManager()
        {
            ProxyViewModel.SetPageManager(this);
        }

        public void ChangeCurrentPage(string pageIdentifier)
        {
            //throw new NotImplementedException();
        }

        public void ShowPage(string pageIdentifier)
        {
            //throw new NotImplementedException();
        }
        
        public void RegisterProperty(ProxyDasaObject dasaObject, string propertyName)
        {
            //throw new NotImplementedException();
        }


        public void ClearRegistrations(string pageIdentifier)
        {
            throw new NotImplementedException();
        }
    }
}
